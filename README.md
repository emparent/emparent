Emparent
=====
Emparent is an open-source Android app that helps you resolve conflicts at home, using [Nonviolent Communication](https://en.wikipedia.org/wiki/Nonviolent_Communication) as the approach it recommends.

See the [official website](https://emparent.gitlab.io/en/) for more information, as well as related resources.

Contribute
====
ANYONE can contribute, no special skills required. [Send a message](https://riot.im/app/#/room/#emparent:matrix.org) if you'd like to help.

You can help by donating photos of exercises, turning photos into low-poly illustrations (easy stuff, anyone can do it), conducting user interviews, doing user testing, ideating, creating graphics, [translating](https://www.transifex.com/feeel/feeel/), spreading the message, programming, or doing anything else that would help the project. :)

Donate
====
You can help keep the project going by **donating at [Liberapay](https://liberapay.com/Emparent/)**!
