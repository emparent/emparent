// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emparent/database/dbhelper.dart';
import 'package:emparent/models/person.dart';
import 'package:flutter/material.dart';

class PersonAvatar extends StatelessWidget {
  static const double OVERLAP_RATIO = 0.1;
  final DBPerson person;
  final double radius;
  static final _MATERIAL_COLORS = [
    Colors.red.shade300,
    Colors.pink.shade300,
    Colors.purple.shade200,
    Colors.deepPurple.shade200,
    Colors.indigo.shade200,
    Colors.blue,
    Colors.lightBlue,
    Colors.cyan,
    Colors.teal.shade400,
    Colors.green,
    Colors.lightGreen,
    Colors.lime,
    Colors.yellow,
    Colors.amber,
    Colors.orange,
    Colors.deepOrange,
    Colors.brown.shade50,
    Colors.blueGrey.shade300
  ];

  PersonAvatar({Key key, @required this.person, this.radius}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var name = person.name;

    Widget avatarChild;
    if (person.avatar != null || name.length == 0) {
      avatarChild = null;
    } else if (person.id == DBHelper.YOU_ID) {
      avatarChild = Icon(Icons.person);
    } else {
      avatarChild = Text(name[0],
          style: Theme.of(context)
              .textTheme
              .title
              .copyWith(color: Colors.black87));
    }

    return CircleAvatar(
        radius: radius, backgroundColor: _getColor(name), child: avatarChild);
  }

  Color _getColor(String name) {
    if (name.length == 0) return Colors.grey.shade300;
    var colorPos = name.codeUnitAt(0) % _MATERIAL_COLORS.length;
    return _MATERIAL_COLORS[colorPos];
  }
}
