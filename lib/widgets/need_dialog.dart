// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emparent/l10n/l10n.dart';
import 'package:emparent/models/need.dart';
import 'package:flutter/material.dart';
import 'package:emparent/models/person.dart';
import 'package:emparent/database/dbhelper.dart';

class NeedDialog extends StatefulWidget {
  final String groupNameTranslation;
  final List<String> groupNeedKeys;
  final int situationId;
  final int personId;
  List<String> selectedKeys;

  NeedDialog(
      {Key key,
      @required this.groupNameTranslation,
      @required this.groupNeedKeys,
      @required this.situationId,
      @required this.personId,
      @required this.selectedKeys})
      : super(key: key);

  @override
  _NeedDialogState createState() => _NeedDialogState();
}

class _NeedDialogState extends State<NeedDialog> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(widget.groupNameTranslation),
      content: Container(
          width: double.maxFinite,
          child: ListView.builder(
              itemCount: widget.groupNeedKeys.length,
              itemBuilder: (context, index) {
                String needKey = widget.groupNeedKeys[index];
                return CheckboxListTile(
                    title: Text(Need.getTranslation(context, needKey)),
                    value: widget.selectedKeys.contains(needKey),
                    onChanged: (bool newValue) async {
                      setState(
                        () {
                          if (newValue) {
                            widget.selectedKeys.add(needKey);
                            DBHelper.db.insertPersonNeed(
                                widget.situationId, widget.personId, needKey);
                          } else {
                            widget.selectedKeys.remove(needKey);
                            DBHelper.db.deletePersonNeed(
                                widget.situationId, widget.personId, needKey);
                          }
                        },
                      );
                    });
              },
              shrinkWrap: true,
              scrollDirection: Axis.vertical)),
      actions: <Widget>[
        FlatButton(
          child: Text(L10n.of(context).btnDone),
          onPressed: () => Navigator.pop(context),
        )
      ],
    );
  }
}
