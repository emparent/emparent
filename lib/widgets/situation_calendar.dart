// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:emparent/models/situation.dart';
import 'package:emparent/database/dbhelper.dart';
import 'package:intl/intl.dart';

class SituationCalendarMenu extends StatefulWidget {
  final DBSituation situation; //todo this will become out of sync with database

  SituationCalendarMenu({Key key, @required this.situation}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SituationCalendarMenuState();
}

class _SituationCalendarMenuState extends State<SituationCalendarMenu> {
  static const int _INIT_YEAR = 2000;
  static const int _YEARS_AHEAD = 1;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      color: Theme.of(context).colorScheme.surface,
      child: Text(DateFormat.yMMMd().format(DateTime.fromMillisecondsSinceEpoch(
          widget.situation.dateMilliseconds))),
      onPressed: () async {
        var selectedDate = await showDatePicker(
          context: context,
          initialDate: DateTime.fromMillisecondsSinceEpoch(
              widget.situation.dateMilliseconds),
          firstDate: DateTime(_INIT_YEAR),
          lastDate: DateTime(DateTime.now().year + _YEARS_AHEAD),
        );
        if (selectedDate != null) {
        int dateMS = selectedDate.millisecondsSinceEpoch;
        widget.situation.dateMilliseconds = dateMS;
        DBHelper.db.updateSituationDate(
            widget.situation.id, dateMS);
        setState(() {});
        }
      },
    );
  }
}
