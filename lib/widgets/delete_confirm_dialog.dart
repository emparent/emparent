// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emparent/l10n/l10n.dart';
import 'package:flutter/material.dart';
import 'package:emparent/database/dbhelper.dart';

class DeleteConfirmDialog extends StatefulWidget {
  final String itemName;
  final String deleteMessage;
  final Function deleteFunction;

  DeleteConfirmDialog(
      {Key key,
      @required this.itemName,
      @required this.deleteMessage,
      @required this.deleteFunction})
      : super(key: key);

  @override
  _DeleteConfirmDialogState createState() => _DeleteConfirmDialogState();
}

class _DeleteConfirmDialogState extends State<DeleteConfirmDialog> {
  bool matchError = false;

  @override
  Widget build(BuildContext context) {
    TextEditingController controller = TextEditingController();
    return AlertDialog(
      title: Text(L10n.of(context).textSureAboutDelete(widget.itemName)),
      content: SingleChildScrollView(
          child: Column(
        children: [
          Padding(
              padding: EdgeInsets.only(top: 8),
              child: Text(
                widget.deleteMessage,
                style: Theme.of(context).textTheme.caption,
              )),
          TextField(
            controller: controller,
            autofocus: true,
            decoration: InputDecoration(
              border: InputBorder.none,
              filled: true,
            ),
          ),
          if (matchError)
            Padding(
                padding: EdgeInsets.only(top: 8),
                child: Text(
                  L10n.of(context).textDoesNotMatchName,
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(color: Colors.red),
                )),
        ],
        mainAxisSize: MainAxisSize.min,
      )),
      actions: <Widget>[
        FlatButton(
          child: Text(L10n.of(context).btnCancel),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        FlatButton(
          child: Text(L10n.of(context).btnDelete),
          onPressed: () {
            if (controller.text.toLowerCase() ==
                widget.itemName.toLowerCase()) {
              widget.deleteFunction();
              Navigator.of(context).pop();
            } else {
              setState(() {
                matchError = true;
              });
            }
          },
        ),
      ],
    );
  }
}
