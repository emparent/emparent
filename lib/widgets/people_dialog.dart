// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emparent/l10n/l10n.dart';
import 'package:emparent/widgets/avatar.dart';
import 'package:emparent/widgets/delete_confirm_dialog.dart';
import 'package:flutter/material.dart';
import 'package:emparent/models/person.dart';
import 'package:emparent/database/dbhelper.dart';

class PeopleDialog extends StatefulWidget {
  @override
  _PeopleDialogState createState() => _PeopleDialogState();
}

class _PeopleDialogState extends State<PeopleDialog> {
  // FocusNode focusNode;

  // @override
  // void initState() {
  //   super.initState();
  //   focusNode = FocusNode();
  // }

  // @override
  // void dispose() {
  //   focusNode.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(L10n.of(context).textManagePeople),
      content: FutureBuilder<List<DBPerson>>(
          future: DBHelper.db.queryAllPeople(),
          builder:
              (BuildContext context, AsyncSnapshot<List<DBPerson>> snapshot) {
            if (snapshot.hasData) {
              return SingleChildScrollView(
                  child: Column(
                      children: _getPersonRows(snapshot
                          .data))); //todo check if there's not a more elegant solution
            } else {
              return Center(
                  child:
                      CircularProgressIndicator()); //todo is this while loading or after loading? the latter wouldn't make sense
            }
          }),
      actions: <Widget>[
        FlatButton(
          child: Text(L10n.of(context).btnDone),
          onPressed: () {
            Navigator.pop(
              context,
            ); //todo !!!
          },
        )
      ],
    );
  }

  List<Widget> _getPersonRows(List<DBPerson> people) {
    String _she = L10n.of(context).textShe;
    String _he = L10n.of(context).textHe;
    return List.generate(people.length + 1, (int i) {
      if (i == people.length) {
        return RaisedButton(
          child: Text(L10n.of(context).btnAddPerson),
          onPressed: () async {
            await DBHelper.db
                .createPerson(); //todo add this person to people added
            setState(() {});
            // FocusScope.of(context).requestFocus(focusNode);
          },
        );
      } else {
        DBPerson person = people[i];

        Widget addPhoto = Padding(
            padding: EdgeInsets.only(right: 8),
            child: PersonAvatar(
              person: person,
            )); //todo CircleAvatar(child: Icon(Icons.add_a_photo)));
        Widget pronounDropdown = DropdownButton<String>(
          items: <String>[_she, _he].map((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
          value: person.she ? _she : _he,
          onChanged: (String newValue) async {
            await DBHelper.db.updatePersonPronoun(person.id, newValue == _she);
            setState(() {});
          },
        );

        if (person.id == DBHelper.YOU_ID) {
          return Row(
            children: <Widget>[
              addPhoto,
              Expanded(child: Text(L10n.of(context).textYou)),
              pronounDropdown
            ],
          );
        } else {
          var controller = TextEditingController(
            //TODO DISPOSE OF THIS TOO!!!!
            text: person.name,
          );

          controller.addListener(() async {
            await DBHelper.db.updatePersonName(person.id, controller.text);
          });

          return Row(
            children: <Widget>[
              addPhoto,
              Expanded(
                  child: TextField(
                decoration: InputDecoration(
                    border: InputBorder.none,
                    filled: true,
                    hintText: L10n.of(context).textName),
                controller: controller,
                // focusNode: (pos == people.length - 1) DOES NOT WORK
                //     ? focusNode
                //     : null, //todo can this be null?,
              )),
              Padding(
                  padding: EdgeInsets.only(left: 8), child: pronounDropdown),
              IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () async {
                    showDialog( //todo show dialog only when the person has been used before
                        context: context,
                        builder: (_) => DeleteConfirmDialog(
                              itemName: person.name,
                              deleteMessage:
                                  L10n.of(context).textDeletePersonMessage,
                              deleteFunction: () async {
                                await DBHelper.db.deletePerson(person.id);
                              },
                            ));
                    setState(() {});
                  })
            ],
          );
        }
      }
    });
  }
}
