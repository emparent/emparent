// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';

class CheckboxChip extends StatelessWidget {
  final Widget label, avatar;
  final bool selected;
  final ValueChanged<bool> onSelected;

  CheckboxChip({this.label, this.avatar, this.selected, this.onSelected});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Checkbox(
          onChanged: onSelected,
          value: selected,
        ),
        ChoiceChip(
          label: label,
          avatar: avatar,
          selected: selected,
          onSelected: onSelected,
        )
      ],
      mainAxisSize: MainAxisSize.min,
    );
  }
}
