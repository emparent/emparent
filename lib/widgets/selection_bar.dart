import 'dart:collection';

import 'package:emotionpicker/emotion.dart';
import 'package:emotionpicker/emotion_display.dart';
import 'package:emparent/database/dbhelper.dart';
import 'package:flutter/material.dart';

class SelectionBar extends StatefulWidget {
  LinkedHashMap<String, dynamic> itemMap;
  Function(dynamic value) getColor;
  Function(dynamic value)
      onItemRemoved; //todo replace this with Function removeItem and getRemovedItems, getAddedItems
  bool darkText;

  SelectionBar(
      {@required this.itemMap,
      this.onItemRemoved,
      this.getColor,
      this.darkText = true});

  @override
  SelectionBarState createState() {
    return SelectionBarState();
  }
}

class SelectionBarState extends State<SelectionBar> {
  @override
  Widget build(BuildContext context) {
    var onChipColor = widget.darkText ? Colors.black87 : Colors.white;
    var labels = widget.itemMap.keys.toList();
    var values = widget.itemMap.values.toList();
    return (widget.itemMap.length > 0)
        ? BottomAppBar(
            child: ListView.builder(
            padding: EdgeInsets.all(8),
            itemCount: widget.itemMap.length,
            itemBuilder: (context, index) {
              return Padding(
                child: Chip(
                  label: Text(
                    labels[index],
                    style: TextStyle(color: onChipColor), //todo from theme
                  ),
                  deleteIconColor: onChipColor, //todo from theme
                  backgroundColor: widget.getColor != null
                      ? widget.getColor(values[index])
                      : Theme.of(context).chipTheme.backgroundColor,
                  onDeleted: () {
                    setState(() {
                      widget.onItemRemoved(values[index]);
                      widget.itemMap.remove(labels[index]);
                    });
                  },
                ),
                padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
              );
            },
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
          ))
        : Container(); //todo figure out what to do with an empty one //todo use an if statement within containing list
  }
}
