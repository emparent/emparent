// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emparent/database/dbhelper.dart';
import 'package:emparent/models/person.dart';
import 'package:emparent/widgets/avatar.dart';
import 'package:flutter/material.dart';

class AvatarRow extends StatelessWidget {
  static const double OVERLAP = 4;
  final List<DBPerson> people;
  final double radius;

  AvatarRow({Key key, @required this.people, this.radius}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
        children: List.generate(people.length, (int pos) {
      if (people[pos].id == DBHelper.YOU_ID) { //HACK: counting on YOU_ID being skipped and part of the list, to skip it, but add an element that gives stack its dimensions
        return Container(
          width: (2 * radius - OVERLAP) * people.length + OVERLAP,
          height: 2 * radius,
        );
      } else
        return Positioned.directional(
            textDirection: TextDirection.ltr,
            start: (2 * radius - OVERLAP) * pos,
            top: 0,
            child: PersonAvatar(
              person: people[pos],
              radius: radius,
            ));
    }));
  }
}
