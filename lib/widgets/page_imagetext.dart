// Copyright (C) 2019 Miroslav Mazel
//
// This file is part of Emparent.
//
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emparent/widgets/inline_app_bar.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:emparent/models/person.dart';
import 'package:emparent/widgets/people_dialog.dart';
import 'package:emparent/database/dbhelper.dart';
import 'package:emparent/models/topic.dart';
import 'package:emparent/models/situation.dart';
import 'package:emparent/screens/page_mixin.dart';
import 'package:emparent/widgets/situation_calendar.dart';

class PageImageText extends StatelessWidget {
  final String title;
  final String text;
  final String bgImage;
  final AlignmentGeometry alignment;

  PageImageText(
      {Key key,
      @required this.title,
      @required this.text,
      @required this.bgImage,
      this.alignment = Alignment.center})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.background,
        image: DecorationImage(
            image: AssetImage(bgImage),
            fit: BoxFit.cover,
            alignment: alignment),
      ),
      child: Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Padding(
                child: Text(
                  title,
                  style: Theme.of(context)
                      .textTheme
                      .title
                      .copyWith(color: Colors.white),
                ),
                padding: EdgeInsets.only(bottom: 16),
              ),
              Text(text,
                  style: Theme.of(context)
                      .textTheme
                      .body1
                      .copyWith(color: Colors.white))
            ],
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
          )),
      alignment: Alignment.bottomLeft,
    );
  }
}
