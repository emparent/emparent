// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:meta/meta.dart';

class DBPerson {
  final int id;
  String name;
  String avatar;
  bool she;

  static final String idCol = "id";
  static final String nameCol = "name";
  static final String avatarCol = "avatar";
  static final String sheCol = "she";

  DBPerson({@required this.id, this.name = "", this.she = true, this.avatar});

  Map<String, dynamic> toMap() => {
    idCol: id,
    nameCol: name,
    sheCol: she ? 1 : 0,
    avatarCol: avatar
  };

  factory DBPerson.fromMap(Map<String, dynamic> map) => DBPerson(
    id: map[idCol],
    name: map[nameCol],
    she: map[sheCol] > 0,
    avatar: map[avatarCol]
  );
}