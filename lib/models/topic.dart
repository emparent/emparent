// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emparent/models/situation.dart';
import 'package:meta/meta.dart';

class DBTopic {
  final int id;
  String title;
  bool resolved;

  static final String idCol = 'id';
  static final String titleCol = 'title';
  static final String resolvedCol = 'resolved';
//  List<Situation> situations;

  DBTopic({@required this.id, this.title = "", this.resolved = false});

  Map<String, dynamic> toMap() => {
    idCol: id,
    titleCol: title,
    resolvedCol: resolved ? 1 : 0
  };

  factory DBTopic.fromMap(Map<String, dynamic> map) => DBTopic(
    id: map[idCol],
    title: map[titleCol],
    resolved: map[resolvedCol] > 0,
  );
}
