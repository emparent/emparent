// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emparent/database/dbhelper.dart';
import 'package:meta/meta.dart';

class DBIdea {
  final int id;
  final int situationId;
  String idea;
  
  DBIdea({@required this.id, @required this.situationId, this.idea = ""});

  Map<String, dynamic> toMap() => {
    DBHelper.idCol: id,
    DBHelper.situationCol: situationId,
    DBHelper.ideaCol: idea
  };

  factory DBIdea.fromMap(Map<String, dynamic> map) => DBIdea(
    id: map[DBHelper.idCol],
    situationId: map[DBHelper.situationCol],
    idea: map[DBHelper.ideaCol],
  );
}