// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emparent/l10n/l10n.dart';
import 'package:flutter/widgets.dart';

class Need {
  static const PHYSIOLOGY = "catPhysiology";
  static const SAFETY = "catSafety";
  static const FREEDOM = "catFreedom";
  static const BELONGING = "catBelonging";
  static const RESPECT = "catRespect";
  static const HARMONY = "catHarmony";
  static const UNDERSTANDING = "catUnderstanding";
  static const ACTUALIZATION = "catSelf_actualization";
  static const PARTICIPATION = "catParticipation";

  static const PHYSIOLOGY_AIR = "air";
  static const PHYSIOLOGY_FOOD = "food";
  static const PHYSIOLOGY_EXERCISE = "movement / exercise";
  static const PHYSIOLOGY_REST = "rest / sleep";
  static const PHYSIOLOGY_HEALLTH = "physical health";
  static const PHYSIOLOGY_SEX = "sexual expression";
  static const PHYSIOLOGY_SAFETY = "physical safety";
  static const PHYSIOLOGY_HOME = "shelter";
  static const PHYSIOLOGY_TOUCH = "touch";
  static const PHYSIOLOGY_WATER = "water";

  static const SAFETY_PEACE = "inner peace";
  static const SAFETY_SAFETY = "safety";
  static const SAFETY_SECURITY = "security";
  static const SAFETY_CERTAINTY = "certainty";
  static const SAFETY_STABILITY = "stability";
  static const SAFETY_SUPPORT = "support";

  static const FREEDOM_CHOICE = "freedom of choice";
  static const FREEDOM_FREEDOM = "freedom";
  static const FREEDOM_SPACE = "space";
  static const FREEDOM_SPONTANEITY = "spontaneity";
  static const FREEDOM_INDEPENDENCE = "independence";

  static const BELONGING_ACCEPTANCE = "acceptance";
  static const BELONGING_AFFECTION = "affection";
  static const BELONGING_BELONGING = "belonging";
  static const BELONGING_COLLABORATION = "collaboration";
  static const BELONGING_COMMUNICATION = "communication";
  static const BELONGING_COMMUNITY = "community";
  static const BELONGING_CLOSENESS = "closeness";
  static const BELONGING_COMPANY = "company";
  static const BELONGING_PARTNERSHIP = "partnership";
  static const BELONGING_COMPASSION = "compassion";
  static const BELONGING_CONSIDERATION = "consideration";
  static const BELONGING_EMPATHY = "empathy";
  static const BELONGING_INCLUSION = "inclusion";
  static const BELONGING_INTIMACY = "intimacy";
  static const BELONGING_LOVE = "love";
  static const BELONGING_RECIPROCITY = "reciprocity";
  static const BELONGING_CARE = "care";
  static const BELONGING_WARMTH = "warmth";

  static const RESPECT_RECOGNITION = "recognition";
  static const RESPECT_COMPETENCE = "competence";
  static const RESPECT_RESPECT = "respect";
  static const RESPECT_KNOWN = "be known";
  static const RESPECT_SEEN = "be seen";
  static const RESPECT_UNDERSTOOD = "be understood";
  static const RESPECT_TRUST = "trust";

  static const HARMONY_FAIRNESS = "fairness";
  static const HARMONY_BEAUTY = "beauty";
  static const HARMONY_CONSISTENCY = "consistency";
  static const HARMONY_EQUALITY = "equality";
  static const HARMONY_HARMONY = "harmony";
  static const HARMONY_ORDER = "order";
  static const HARMONY_QUIET = "quiet";

  static const UNDERSTANDING_AUTHENTICITY = "authenticity";
  static const UNDERSTANDING_HONESTY = "honesty";
  static const UNDERSTANDING_AWARENESS = "awareness";
  static const UNDERSTANDING_CLARITY = "clarity";
  static const UNDERSTANDING_DISCOVERY = "discovery";
  static const UNDERSTANDING_INTEGRITY = "integrity";
  static const UNDERSTANDING_LEARNING = "learning";
  static const UNDERSTANDING_PRESENCE = "presence";
  static const UNDERSTANDING_UNDRESTAND = "to understand";
  static const UNDERSTANDING_KNOW = "to know";

  static const ACTUALIZATION_LIFE_CELEBRATION = "celebration of life";
  static const ACTUALIZATION_CHALLENGE = "challenge";
  static const ACTUALIZATION_CONSCIOUSNESS = "consciousness";
  static const ACTUALIZATION_CREATIVITY = "creativity";
  static const ACTUALIZATION_EASE = "ease";
  static const ACTUALIZATION_GROWTH = "growth";
  static const ACTUALIZATION_HOPE = "hope";
  static const ACTUALIZATION_INSPIRATION = "inspiration";
  static const ACTUALIZATION_JOY = "joy";
  static const ACTUALIZATION_HUMOR = "humor";
  static const ACTUALIZATION_MOURNING = "mourning";
  static const ACTUALIZATION_SELF_EXPRESSION = "self-expression";
  static const ACTUALIZATION_PLAYFULNESS = "playfulness";
  static const ACTUALIZATION_STIMULATION = "stimulation";

  static const PARTICIPATION_CONTRIBUTION = "contribution";
  static const PARTICIPATION_EFFICACY = "efficacy";
  static const PARTICIPATION_EFFECTIVENESS = "effectiveness";
  static const PARTICIPATION_PARTICIPATION = "participation";
  static const PARTICIPATION_PURPOSE = "purpose";
  static const PARTICIPATION_MATTERING = "to matter";
  static const PARTICIPATION_UNDERSTANDING = "to be understood";

  static String getTranslation(BuildContext context, String key) {
    switch (key) {
      case PHYSIOLOGY:
        return L10n.of(context).needPhysiology;
      case SAFETY:
        return L10n.of(context).needSafety;
      case FREEDOM:
        return L10n.of(context).needFreedom;
      case BELONGING:
        return L10n.of(context).needBelonging;
      case RESPECT:
        return L10n.of(context).needRespect;
      case HARMONY:
        return L10n.of(context).needHarmony;
      case UNDERSTANDING:
        return L10n.of(context).needUnderstanding;
      case ACTUALIZATION:
        return L10n.of(context).needSelfActualization;
      case PARTICIPATION:
        return L10n.of(context).needParticipation;

      case PHYSIOLOGY_AIR:
        return L10n.of(context).needPhysiologyAir;
      case PHYSIOLOGY_FOOD:
        return L10n.of(context).needPhysiologyFood;
      case PHYSIOLOGY_EXERCISE:
        return L10n.of(context).needPhysiologyExercise;
      case PHYSIOLOGY_REST:
        return L10n.of(context).needPhysiologyRest;
      case PHYSIOLOGY_HEALLTH:
        return L10n.of(context).needPhysiologyHeallth;
      case PHYSIOLOGY_SEX:
        return L10n.of(context).needPhysiologySex;
      case PHYSIOLOGY_SAFETY:
        return L10n.of(context).needPhysiologySafety;
      case PHYSIOLOGY_HOME:
        return L10n.of(context).needPhysiologyHome;
      case PHYSIOLOGY_TOUCH:
        return L10n.of(context).needPhysiologyTouch;
      case PHYSIOLOGY_WATER:
        return L10n.of(context).needPhysiologyWater;

      case SAFETY_PEACE:
        return L10n.of(context).needSafetyPeace;
      case SAFETY_SAFETY:
        return L10n.of(context).needSafetySafety;
      case SAFETY_SECURITY:
        return L10n.of(context).needSafetySecurity;
      case SAFETY_CERTAINTY:
        return L10n.of(context).needSafetyCertainty;
      case SAFETY_STABILITY:
        return L10n.of(context).needSafetyStability;
      case SAFETY_SUPPORT:
        return L10n.of(context).needSafetySupport;

      case FREEDOM_CHOICE:
        return L10n.of(context).needFreedomChoice;
      case FREEDOM_FREEDOM:
        return L10n.of(context).needFreedomFreedom;
      case FREEDOM_SPACE:
        return L10n.of(context).needFreedomSpace;
      case FREEDOM_SPONTANEITY:
        return L10n.of(context).needFreedomSpontaneity;
      case FREEDOM_INDEPENDENCE:
        return L10n.of(context).needFreedomIndependence;

      case BELONGING_ACCEPTANCE:
        return L10n.of(context).needBelongingAcceptance;
      case BELONGING_AFFECTION:
        return L10n.of(context).needBelongingAffection;
      case BELONGING_BELONGING:
        return L10n.of(context).needBelongingBelonging;
      case BELONGING_COLLABORATION:
        return L10n.of(context).needBelongingCollaboration;
      case BELONGING_COMMUNICATION:
        return L10n.of(context).needBelongingCommunication;
      case BELONGING_COMMUNITY:
        return L10n.of(context).needBelongingCommunity;
      case BELONGING_CLOSENESS:
        return L10n.of(context).needBelongingCloseness;
      case BELONGING_COMPANY:
        return L10n.of(context).needBelongingCompany;
      case BELONGING_PARTNERSHIP:
        return L10n.of(context).needBelongingPartnership;
      case BELONGING_COMPASSION:
        return L10n.of(context).needBelongingCompassion;
      case BELONGING_CONSIDERATION:
        return L10n.of(context).needBelongingConsideration;
      case BELONGING_EMPATHY:
        return L10n.of(context).needBelongingEmpathy;
      case BELONGING_INCLUSION:
        return L10n.of(context).needBelongingInclusion;
      case BELONGING_INTIMACY:
        return L10n.of(context).needBelongingIntimacy;
      case BELONGING_LOVE:
        return L10n.of(context).needBelongingLove;
      case BELONGING_RECIPROCITY:
        return L10n.of(context).needBelongingReciprocity;
      case BELONGING_CARE:
        return L10n.of(context).needBelongingCare;
      case BELONGING_WARMTH:
        return L10n.of(context).needBelongingWarmth;

      case RESPECT_RECOGNITION:
        return L10n.of(context).needRespectRecognition;
      case RESPECT_COMPETENCE:
        return L10n.of(context).needRespectCompetence;
      case RESPECT_RESPECT:
        return L10n.of(context).needRespectRespect;
      case RESPECT_KNOWN:
        return L10n.of(context).needRespectKnown;
      case RESPECT_SEEN:
        return L10n.of(context).needRespectSeen;
      case RESPECT_UNDERSTOOD:
        return L10n.of(context).needRespectUnderstood;
      case RESPECT_TRUST:
        return L10n.of(context).needRespectTrust;

      case HARMONY_FAIRNESS:
        return L10n.of(context).needHarmonyFairness;
      case HARMONY_BEAUTY:
        return L10n.of(context).needHarmonyBeauty;
      case HARMONY_CONSISTENCY:
        return L10n.of(context).needHarmonyConsistency;
      case HARMONY_EQUALITY:
        return L10n.of(context).needHarmonyEquality;
      case HARMONY_HARMONY:
        return L10n.of(context).needHarmonyHarmony;
      case HARMONY_ORDER:
        return L10n.of(context).needHarmonyOrder;
      case HARMONY_QUIET:
        return L10n.of(context).needHarmonyQuiet;

      case UNDERSTANDING_AUTHENTICITY:
        return L10n.of(context).needUnderstandingAuthenticity;
      case UNDERSTANDING_HONESTY:
        return L10n.of(context).needUnderstandingHonesty;
      case UNDERSTANDING_AWARENESS:
        return L10n.of(context).needUnderstandingAwareness;
      case UNDERSTANDING_CLARITY:
        return L10n.of(context).needUnderstandingClarity;
      case UNDERSTANDING_DISCOVERY:
        return L10n.of(context).needUnderstandingDiscovery;
      case UNDERSTANDING_INTEGRITY:
        return L10n.of(context).needUnderstandingIntegrity;
      case UNDERSTANDING_LEARNING:
        return L10n.of(context).needUnderstandingLearning;
      case UNDERSTANDING_PRESENCE:
        return L10n.of(context).needUnderstandingPresence;
      case UNDERSTANDING_UNDRESTAND:
        return L10n.of(context).needUnderstandingUnderstand;
      case UNDERSTANDING_KNOW:
        return L10n.of(context).needUnderstandingKnow;

      case ACTUALIZATION_LIFE_CELEBRATION:
        return L10n.of(context).needActualizationLifeCelebration;
      case ACTUALIZATION_CHALLENGE:
        return L10n.of(context).needActualizationChallenge;
      case ACTUALIZATION_CONSCIOUSNESS:
        return L10n.of(context).needActualizationConsciousness;
      case ACTUALIZATION_CREATIVITY:
        return L10n.of(context).needActualizationCreativity;
      case ACTUALIZATION_EASE:
        return L10n.of(context).needActualizationEase;
      case ACTUALIZATION_GROWTH:
        return L10n.of(context).needActualizationGrowth;
      case ACTUALIZATION_HOPE:
        return L10n.of(context).needActualizationHope;
      case ACTUALIZATION_INSPIRATION:
        return L10n.of(context).needActualizationInspiration;
      case ACTUALIZATION_JOY:
        return L10n.of(context).needActualizationJoy;
      case ACTUALIZATION_HUMOR:
        return L10n.of(context).needActualizationHumor;
      case ACTUALIZATION_MOURNING:
        return L10n.of(context).needActualizationMourning;
      case ACTUALIZATION_SELF_EXPRESSION:
        return L10n.of(context).needActualizationSelfExpression;
      case ACTUALIZATION_PLAYFULNESS:
        return L10n.of(context).needActualizationPlayfulness;
      case ACTUALIZATION_STIMULATION:
        return L10n.of(context).needActualizationStimulation;

      case PARTICIPATION_CONTRIBUTION:
        return L10n.of(context).needParticipationContribution;
      case PARTICIPATION_EFFICACY:
        return L10n.of(context).needParticipationEfficacy;
      case PARTICIPATION_EFFECTIVENESS:
        return L10n.of(context).needParticipationEffectiveness;
      case PARTICIPATION_PARTICIPATION:
        return L10n.of(context).needParticipationParticipation;
      case PARTICIPATION_PURPOSE:
        return L10n.of(context).needParticipationPurpose;
      case PARTICIPATION_MATTERING:
        return L10n.of(context).needParticipationMattering;
      case PARTICIPATION_UNDERSTANDING:
        return L10n.of(context).needParticipationUnderstanding;

      default:
        return key;
    }
  }

  static Map<String, List<String>> needTranslationKeys = {
    PHYSIOLOGY: [
      PHYSIOLOGY_AIR,
      PHYSIOLOGY_FOOD,
      PHYSIOLOGY_EXERCISE,
      PHYSIOLOGY_REST,
      PHYSIOLOGY_HEALLTH,
      PHYSIOLOGY_SEX,
      PHYSIOLOGY_SAFETY,
      PHYSIOLOGY_HOME,
      PHYSIOLOGY_TOUCH,
      PHYSIOLOGY_WATER
    ],
    SAFETY: [
      SAFETY_PEACE,
      SAFETY_SAFETY,
      SAFETY_SECURITY,
      SAFETY_CERTAINTY,
      SAFETY_STABILITY,
      SAFETY_SUPPORT,
    ],
    FREEDOM: [
      FREEDOM_CHOICE,
      FREEDOM_FREEDOM,
      FREEDOM_SPACE,
      FREEDOM_SPONTANEITY,
      FREEDOM_INDEPENDENCE,
    ],
    BELONGING: [
      BELONGING_ACCEPTANCE,
      BELONGING_AFFECTION,
      BELONGING_BELONGING,
      BELONGING_COLLABORATION,
      BELONGING_COMMUNICATION,
      BELONGING_COMMUNITY,
      BELONGING_CLOSENESS,
      BELONGING_COMPANY,
      BELONGING_PARTNERSHIP,
      BELONGING_COMPASSION,
      BELONGING_CONSIDERATION,
      BELONGING_EMPATHY,
      BELONGING_INCLUSION,
      BELONGING_INTIMACY,
      BELONGING_LOVE,
      BELONGING_RECIPROCITY,
      BELONGING_CARE,
      BELONGING_WARMTH,
    ],
    RESPECT: [
      RESPECT_RECOGNITION,
      RESPECT_COMPETENCE,
      RESPECT_RESPECT,
      RESPECT_KNOWN,
      RESPECT_SEEN,
      RESPECT_UNDERSTOOD,
      RESPECT_TRUST,
    ],
    UNDERSTANDING: [
      UNDERSTANDING_AUTHENTICITY,
      UNDERSTANDING_HONESTY,
      UNDERSTANDING_AWARENESS,
      UNDERSTANDING_CLARITY,
      UNDERSTANDING_DISCOVERY,
      UNDERSTANDING_INTEGRITY,
      UNDERSTANDING_LEARNING,
      UNDERSTANDING_PRESENCE,
      UNDERSTANDING_UNDRESTAND,
      UNDERSTANDING_KNOW,
    ],
    HARMONY: [
      HARMONY_FAIRNESS,
      HARMONY_BEAUTY,
      HARMONY_CONSISTENCY,
      HARMONY_EQUALITY,
      HARMONY_HARMONY,
      HARMONY_ORDER,
      HARMONY_QUIET,
    ],
    ACTUALIZATION: [
      ACTUALIZATION_LIFE_CELEBRATION,
      ACTUALIZATION_CHALLENGE,
      ACTUALIZATION_CONSCIOUSNESS,
      ACTUALIZATION_CREATIVITY,
      ACTUALIZATION_EASE,
      ACTUALIZATION_GROWTH,
      ACTUALIZATION_HOPE,
      ACTUALIZATION_INSPIRATION,
      ACTUALIZATION_JOY,
      ACTUALIZATION_HUMOR,
      ACTUALIZATION_MOURNING,
      ACTUALIZATION_SELF_EXPRESSION,
      ACTUALIZATION_PLAYFULNESS,
      ACTUALIZATION_STIMULATION,
    ],
    PARTICIPATION: [
      PARTICIPATION_CONTRIBUTION,
      PARTICIPATION_EFFICACY,
      PARTICIPATION_EFFECTIVENESS,
      PARTICIPATION_PARTICIPATION,
      PARTICIPATION_PURPOSE,
      PARTICIPATION_MATTERING,
      PARTICIPATION_UNDERSTANDING,
    ]
  };
}
