// Copyright (C) 2019 Miroslav Mazel
//
// This file is part of Emparent.
//
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emotionpicker/generated/i18n.dart';
import 'package:emparent/database/sphelper.dart';
import 'package:emparent/screens/main_navigator.dart';
import 'package:emparent/screens/onboarding.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:emparent/screens/overview_topics.dart';
import 'package:emparent/l10n/l10n.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var emotionI18n = I18n.delegate;

    return MaterialApp(
        title: 'Emparent',
        theme: getNightTheme(),
        routes: {
          '/': (context) => MainNavigator(),
          MainNavigator.ONBOARDING: (context) => Onboarding(),
          MainNavigator.HOME: (context) => OverviewTopics(),
        },
        localizationsDelegates: [
          emotionI18n,
          const L10nDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: L10n.SUPPORTED_LOCALES,
        localeResolutionCallback: emotionI18n.resolution(
            fallback: new Locale("en",
                "US")) //todo perhaps replace with something more app-focused?
        );
  }

  ThemeData getNightTheme() {
    var colors = ColorScheme(
        primary: Color(0xFF99B433),
        primaryVariant: Color(0xFF99B433),
        secondary: Color(0xffE96216),
        secondaryVariant: Color(0xFFE96216),
        surface: Color(0xFF4d4d4d),
        background: Color(0xFF333333),
        error: Color(0xFFB00020),
        onPrimary: Colors.white,
        onSecondary: Colors.white,
        onSurface: Colors.white,
        onBackground: Colors.white,
        onError: Colors.white,
        brightness: Brightness.dark);

    return ThemeData(
      brightness: colors.brightness,
      colorScheme: colors,
      primaryColor: colors.primary,
      accentColor: colors.secondary,
      scaffoldBackgroundColor: colors.background,
      cardColor: colors.surface,
      errorColor: colors.error,
      buttonColor: colors.primary,
      toggleableActiveColor: colors.primary,
      textSelectionHandleColor: colors.secondary,
      fontFamily: 'Fira Sans',
    );
  }

  ThemeData getDayTheme() {
    var colors = ColorScheme(
        primary: Color(0xFF7EB45D),
        primaryVariant: Color(0xFF3E6937),
        secondary: Color(0xFFF5D500),
        secondaryVariant: Color(0xFFF47800),
        surface: Color(0xFFFFEDC3),
        background: Color(0xFFFCF1D7),
        error: Color(0xFFB00020),
        onPrimary: Colors.black87,
        onSecondary: Colors.black87,
        onSurface: Colors.black87,
        onBackground: Colors.black87,
        onError: Colors.white,
        brightness: Brightness.light);

    return ThemeData(
      brightness: colors.brightness,
      colorScheme: colors,

      // colors as a replacement for now
      primaryColor: colors.primary,
      accentColor: colors.secondary,
      scaffoldBackgroundColor: colors.background,
      cardColor: colors.surface,
      errorColor: colors.error,

      fontFamily: 'Fira Sans',
    );
  }
}
