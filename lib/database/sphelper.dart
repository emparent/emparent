// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:shared_preferences/shared_preferences.dart';

class SPHelper {
  static const _SHOW_ONBOARDING_KEY = "onboarding";

  static Future<bool> showingOnboarding() async {
    var prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_SHOW_ONBOARDING_KEY) ?? true;
  }

  static Future<void> setShowOnboarding(bool show) async {
    var prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_SHOW_ONBOARDING_KEY, show);
  }
}
