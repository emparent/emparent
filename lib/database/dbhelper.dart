// Copyright (C) 2019 Miroslav Mazel
//
// This file is part of Emparent.
//
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:async';
import 'dart:io';
import 'package:emotionpicker/emotion.dart';
import 'package:emparent/models/idea.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:emparent/models/situation.dart';
import 'package:emparent/models/topic.dart';
import 'package:emparent/models/person.dart';
import 'package:emparent/models/situation_person.dart';

class DBHelper {
  DBHelper._();
  static final DBHelper db = DBHelper._();
  static Database _database;

  static final String _dbFile = "emparent.db";
  static final String _topicTable = 'topics';
  static final String _situationTable = 'situations';
  static final String _personTable = 'people';
  static final String _sitPerTable = 'sit_people';
  static final String _perEmoTable = 'per_emotions';
  static final String _perNeedTable = 'per_needs';
  static final String _ideaTable = 'ideas';

  static const String idCol = 'id';
  static const String situationCol = 'situation';
  static const String personCol = 'person';
  static const String needCol = 'need';
  static const String emotionCol = 'emotion';
  static const String emoIntensityCol = 'intensity';
  static const String ownEmotionDescCol = 'own_emotion_desc';
  static const String ownNeedDescCol = 'own_need_desc';
  static const String ideaCol = 'idea';

  _createDB() async {
    String path = await getPath();

    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE $_topicTable ( "
          "${DBTopic.idCol} INTEGER PRIMARY KEY, "
          "${DBTopic.titleCol} TEXT NOT NULL, "
          "${DBTopic.resolvedCol} INTEGER NOT NULL"
          ")");

      await db.execute("CREATE TABLE $_situationTable ("
          "${DBSituation.idCol} INTEGER PRIMARY KEY, "
          "${DBSituation.descriptionCol} TEXT, "
          "${DBSituation.dateCol} INTEGER NOT NULL, "
          "${DBSituation.topicIdCol} INTEGER NOT NULL"
          ")");

      await db.execute("CREATE TABLE $_personTable ("
          "${DBPerson.idCol} INTEGER PRIMARY KEY, "
          "${DBPerson.nameCol} TEXT NOT NULL, "
          "${DBPerson.avatarCol} TEXT, "
          "${DBPerson.sheCol} INTEGER NOT NULL"
          ")");

      await db.execute("CREATE TABLE $_sitPerTable ("
          "${situationCol} INTEGER NOT NULL, "
          "${personCol} INTEGER NOT NULL, "
          "${ownEmotionDescCol} INTEGER NOT NULL, "
          "${ownNeedDescCol} INTEGER NOT NULL, "
          "PRIMARY KEY(${situationCol},${personCol})"
          ")");

      await db.execute("CREATE TABLE $_perEmoTable ("
          "${situationCol} INTEGER NOT NULL, "
          "${personCol} INTEGER NOT NULL, "
          "${emotionCol} INTEGER NOT NULL, "
          "${emoIntensityCol} INTEGER NOT NULL, "
          "PRIMARY KEY($situationCol,$personCol, $emotionCol, $emoIntensityCol)"
          ")");

      await db.execute("CREATE TABLE $_perNeedTable ("
          "${situationCol} INTEGER NOT NULL, "
          "${personCol} INTEGER NOT NULL, "
          "${needCol} TEXT NOT NULL, "
          "PRIMARY KEY($situationCol,$personCol, $needCol)"
          ")");

      await db.execute("CREATE TABLE $_ideaTable ("
          "${idCol} INTEGER PRIMARY KEY,"
          "${situationCol} INTEGER NOT NULL, "
          "${ideaCol} TEXT NOT NULL"
          ")");

      db.insert(
          _personTable,
          DBPerson(id: YOU_ID, name: "YOU")
              .toMap()); //todo cleaner implementation?
    });
  }

  Future<String> getPath() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    return join(documentsDirectory.path, _dbFile);
  }

  Future<void> exportDB(String exportPath) async {
    if (_database != null) {
      await _database.close();
      _database = await _createDB();

      String dbPath = await getPath();
      File dbFile = File(dbPath);

      dbFile.copy(
          exportPath); //todo think about how to better avoid problems with duplicate filenames
      print(exportPath);
    }
  }

  Future<void> deleteExportedColumns(dbPath) async {
    var tempDb = await openDatabase(dbPath, version: 1, onOpen: (db) {});
    await tempDb.update(_topicTable, {DBTopic.titleCol: ""});
    await tempDb.update(_situationTable, {DBSituation.descriptionCol: ""});
    await tempDb.update(_ideaTable, {ideaCol: ""});
    await tempDb.close();
  }

  static const YOU_ID = 1;

  Future<Database> get database async {
    if (_database == null) {
      _database = await _createDB();
    }

    return _database;
  }

  Future<int> _getNextId(db, tableName, idColumn) async {
    var table = await db
        .rawQuery("SELECT MAX($idColumn)+1 as $idColumn FROM $tableName");
    // if (Sqflite.firstIntValue(table) == 0) return 1; //todo does this work right?
    return table.first[idColumn] == null ? 1 : table.first[idColumn];
  }

// TOPIC

  Future<DBTopic> createTopic() async {
    //todo refactor so that field names are in this file
    final db = await database;
    var table = _topicTable;
    int id = await _getNextId(db, table, DBTopic.idCol);

    DBTopic topic = DBTopic(id: id);

    await db.insert(table, topic.toMap());

    return topic;
  }

  Future<List<DBTopic>> queryAllTopics() async {
    final db = await database;
    var res = await db.query(_topicTable);
    List<DBTopic> list = res.isNotEmpty
        ? res.map((topic) => DBTopic.fromMap(topic)).toList()
        : [];
    return list;
  }

  Future<List<DBTopic>> queryUnresolvedTopics() async {
    final db = await database;
    var res = await db.query(_topicTable, where: "${DBTopic.resolvedCol} = 0");
    List<DBTopic> list = res.isNotEmpty
        ? res.map((topic) => DBTopic.fromMap(topic)).toList()
        : [];
    return list;
  }

  queryTopic(int id) async {
    //todo from map
    final db = await database;
    Map<String, dynamic> res = (await db
            .query(_topicTable, where: "${DBTopic.idCol} = ?", whereArgs: [id]))
        .first;

    return res.isEmpty
        ? Null
        : DBTopic(
            id: res[DBTopic.idCol],
            title: res[DBTopic.titleCol],
            resolved: res[DBTopic.resolvedCol]);
  }

  Future<int> updateTopic(DBTopic topic) async {
    final db = await database;
    return await db.update(
        _topicTable,
        {
          DBTopic.titleCol: topic.title,
          DBTopic.resolvedCol: topic.resolved,
        },
        where: "${DBTopic.idCol} = ?",
        whereArgs: [topic.id]);
  }

  updateTopicTitle(int id, String text) async {
    final db = await database;
    return await db.update(_topicTable, {DBTopic.titleCol: text},
        where: "${DBTopic.idCol} = ?", whereArgs: [id]);
  }

  updateTopicResolution(int id, bool resolved) async {
    final db = await database;
    return await db.update(_topicTable, {DBTopic.resolvedCol: resolved ? 1 : 0},
        where: "${DBTopic.idCol} = ?", whereArgs: [id]);
  }

  Future<void> deleteTopic(int id) async {
    final db = await database;

    List<DBSituation> sits = await queryTopicSituations(
        id); //todo query just int, this is needleessly inefficient

    for (DBSituation sit in sits) {
      await deleteSituation(sit.id);
    }

    db.delete(_topicTable, where: "${DBTopic.idCol} = ?", whereArgs: [id]);
  }

// SITUATION

  Future<DBSituation> createSituation(int topicId) async {
    final db = await database;
    var table = _situationTable;
    int id = await _getNextId(db, table, DBSituation.idCol);
    //todo query topicId

    DBSituation situation = DBSituation(id: id, topicId: topicId);

    await db.insert(table, situation.toMap());

    return situation;
  }

  Future<int> updateSituationDescription(int id, String text) async {
    final db = await database;
    return await db.update(_situationTable, {DBSituation.descriptionCol: text},
        where: "${DBSituation.idCol} = ?", whereArgs: [id]);
  }

  Future<int> updateSituationDate(int id, int date) async {
    final db = await database;
    return await db.update(_situationTable, {DBSituation.dateCol: date},
        where: "${DBSituation.idCol} = ?", whereArgs: [id]);
  }

  Future<List<DBSituation>> queryTopicSituations(int topicId) async {
    final db = await database;

    var res = await db.query(_situationTable,
        where: "${DBSituation.topicIdCol} = ?",
        whereArgs: [topicId],
        orderBy: "${DBSituation.dateCol} ASC");

    List<DBSituation> list = res.isNotEmpty
        ? res.map((situation) => DBSituation.fromMap(situation)).toList()
        : [];
    return list;
  }

  querySituation(int id) async {
    final db = await database;
    var res = await db.query(_situationTable,
        where: "${DBSituation.idCol} = ?", whereArgs: [id]);
    return res.isNotEmpty ? DBSituation.fromMap(res.first) : Null;
  }

  Future<void> deleteSituation(int id) async {
    final db = await database;
    db.delete(_sitPerTable, where: "${situationCol} = ?", whereArgs: [id]);
    db.delete(_perEmoTable, where: "${situationCol} = ?", whereArgs: [id]);
    db.delete(_ideaTable, where: "${situationCol} = ?", whereArgs: [id]);
    db.delete(_perNeedTable, where: "${situationCol} = ?", whereArgs: [id]);

    db.delete(_situationTable,
        where: "${DBSituation.idCol} = ?", whereArgs: [id]);

    return;
  }

// PERSON
  Future<DBPerson> createPerson() async {
    final db = await database;
    var table = _personTable;
    int id = await _getNextId(db, table, DBSituation.idCol);
    DBPerson person = DBPerson(id: id);

    await db.insert(table, person.toMap());

    return person;
  }

  Future<List<DBPerson>> queryAllPeople() async {
    final db = await database;
    var res = await db.query(_personTable);
    List<DBPerson> list = res.isNotEmpty
        ? res.map((person) => DBPerson.fromMap(person)).toList()
        : [];
    return list;
  }

  Future<DBPerson> queryPerson(int id) async {
    final db = await database;
    var res = await db
        .query(_personTable, where: "${DBPerson.idCol} = ?", whereArgs: [id]);
    return res.isNotEmpty ? DBPerson.fromMap(res.first) : Null;
  }

  Future<int> updatePerson(DBPerson person) async {
    final db = await database;
    return await db.update(_personTable, person.toMap(),
        where: "${DBPerson.idCol} = ?", whereArgs: [person.id]);
  }

  Future<int> updatePersonName(int id, String text) async {
    final db = await database;

    return await db.update(_personTable, {DBPerson.nameCol: text},
        where: "${DBPerson.idCol} = ?", whereArgs: [id]);
  }

  Future<int> updatePersonPronoun(int id, bool she) async {
    final db = await database;
    return await db.update(_personTable, {DBPerson.sheCol: she ? 1 : 0},
        where: "${DBPerson.idCol} = ?", whereArgs: [id]);
  }

  Future<void> deletePerson(int id) async {
    final db = await database;

    db.delete(_perEmoTable, where: "${personCol} = ?", whereArgs: [id]);
    db.delete(_perNeedTable, where: "${personCol} = ?", whereArgs: [id]);
    db.delete(_sitPerTable, where: "${personCol} = ?", whereArgs: [id]);
    db.delete(_personTable, where: "${DBPerson.idCol} = ?", whereArgs: [id]);
  }

  // SITUATION PERSON
  Future<void> insertSituationPerson(int situationId, int personId) async {
    final db = await database;

    await db.insert(
        _sitPerTable,
        {
          situationCol: situationId,
          personCol: personId,
          ownEmotionDescCol: 0,
          ownNeedDescCol: 0
        },
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  Future<void> updateSitPerOwnEmotion(
      int situationId, int personId, bool ownEmotion) async {
    final db = await database;

    return await db.update(
        _sitPerTable, {ownEmotionDescCol: ownEmotion ? 1 : 0},
        where: "$situationCol = ? AND $personCol = ?",
        whereArgs: [situationId, personId]);
  }

  Future<void> updateSitPerOwnNeed(
      int situationId, int personId, bool ownNeed) async {
    final db = await database;

    return await db.update(_sitPerTable, {ownNeedDescCol: ownNeed ? 1 : 0},
        where: "$situationCol = ? AND $personCol = ?",
        whereArgs: [situationId, personId]);
  }

  Future<bool> querySituationPerson(int situationId, int personId) async {
    final db = await database;

    var res = await db.query(_sitPerTable,
        where: "$situationCol = ? AND $personCol = ?",
        whereArgs: [situationId, personId]);
    return res.isNotEmpty;
  }

  Future<List<DBPersonInSituation>> queryPeopleInSituation(
      int situationId) async {
    final db = await database;

    var res = await db.query(_sitPerTable,
        where: "$situationCol = ?", whereArgs: [situationId]);

    List<Future<DBPersonInSituation>> list = res.isNotEmpty
        ? res
            .map((item) async {
              DBPerson person = await queryPerson(item[personCol]);
              return DBPersonInSituation(
                  person: person,
                  ownEmotionDesc: item[ownEmotionDescCol] == 1,
                  ownNeedDesc: item[ownNeedDescCol] == 1);
            })
            .cast<Future<DBPersonInSituation>>()
            .toList()
        : [];
    return Future.wait(list);
  }

  Future<List<DBPersonInSituation>> queryPeopleInSituations(
      List<int> situationIds) async {
    final db = await database;

    if (situationIds.length == 0) return null;

    var res = situationIds.length > 1
        ? await db.query(_sitPerTable,
            distinct: true,
            columns: ["$personCol"],
            where: "${situationCol} IN (${List.filled(situationIds.length, "?").join(",")})",
            whereArgs: situationIds)
        : await db.query(_sitPerTable,
            columns: ["$personCol"],
            where: "${situationCol} = ?",
            whereArgs: [situationIds[0]]); //todo check

    List<Future<DBPersonInSituation>> list = res.isNotEmpty
        ? res
            .map((item) async {
              DBPerson person = await queryPerson(item[personCol]);
              return DBPersonInSituation(
                  person: person,
                  ownEmotionDesc: item[ownEmotionDescCol] == 1,
                  ownNeedDesc: item[ownNeedDescCol] == 1);
            })
            .cast<Future<DBPersonInSituation>>()
            .toList()
        : [];
    return Future.wait(list);
  }

  Future<List<int>> queryPeopleIdsBySituation(int situationId) async {
    final db = await database;

    var res = await db.query(_sitPerTable,
        where: "${situationCol} = ?", whereArgs: [situationId]);

    List<int> list = res.isNotEmpty
        ? res.map((item) => (item[personCol])).cast<int>().toList()
        : [];
    return list;
  }

  Future<void> deleteSituationPerson(int situationId, int personId) async {
    final db = await database;

    await db.delete(_sitPerTable,
        where: "${personCol} = ? AND ${situationCol} = ?",
        whereArgs: [personId, situationId]);
  }

  // SITUATION PERSON EMOTIONS
  Future<void> insertPersonEmotion(
      int situationId, int personId, int emotion, int intensity) async {
    final db = await database;

    db.insert(
        _perEmoTable,
        {
          situationCol: situationId,
          personCol: personId,
          emotionCol: emotion,
          emoIntensityCol: intensity
        },
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  Future<void> deletePersonEmotion(
      int situationId, int personId, int emotion, int intensity) async {
    final db = await database;

    db.delete(_perEmoTable,
        where:
            "$situationCol = ? AND $personCol = ? AND $emotionCol = ? AND $emoIntensityCol = ?",
        whereArgs: [situationId, personId, emotion, intensity]);
  }

  Future<List<Emotion>> queryEmotionsBySitPerson(
      int situationId, int personId) async {
    final db = await database;

    var res = await db.query(_perEmoTable,
        where: "$situationCol = ? AND $personCol = ?",
        whereArgs: [situationId, personId]);

    List<Emotion> emotionList = res.isNotEmpty //todo this
        ? res
            .map((item) => Emotion(
                emotion: item[emotionCol], intensity: item[emoIntensityCol]))
            .toList()
        : List<Emotion>();
    return emotionList;
  }

  // SITUATION PERSON NEEDS
  Future<void> insertPersonNeed(
      int situationId, int personId, String need) async {
    final db = await database;

    db.insert(_perNeedTable,
        {situationCol: situationId, personCol: personId, needCol: need},
        conflictAlgorithm: ConflictAlgorithm.ignore);
  }

  Future<void> deletePersonNeed(
      int situationId, int personId, String need) async {
    final db = await database;

    db.delete(_perNeedTable,
        where: "$situationCol = ? AND $personCol = ? AND $needCol = ?",
        whereArgs: [situationId, personId, need]);
    //todo
  }

  Future<List<String>> queryNeedsBySitPerson(
      int situationId, int personId) async {
    final db = await database;

    var res = await db.query(_perNeedTable,
        where: "$situationCol = ? AND $personCol = ?",
        whereArgs: [situationId, personId]);

    List<String> needList = res.isNotEmpty //todo this
        ? res.map((item) => item[needCol]).cast<String>().toList()
        : List<String>();
    return needList;
  }

  // SITUATION IDEA
  Future<void> insertIdea(int situationId, String idea) async {
    final db = await database;
    final table = _ideaTable;
    final int id = await _getNextId(db, table, idCol);

    db.insert(table, {idCol: id, situationCol: situationId, ideaCol: idea});
  }

  void deleteIdea(int id) async {
    final db = await database;

    db.delete(_ideaTable, where: "$idCol = ?", //todo make sure this is safe!!!
        whereArgs: [id]);
  }

  Future<List<DBIdea>> queryIdeasBySituation(int situationId) async {
    final db = await database;

    var res = await db.query(_ideaTable,
        where: "$situationCol = ?", whereArgs: [situationId]);

    List<DBIdea> ideaList = res.isNotEmpty //todo this
        ? res.map((item) => DBIdea.fromMap(item)).toList()
        : [];
    return ideaList;
  }

  Future<void> updateIdeaStr(int id, String ideaStr) async {
    final db = await database;
    return await db.update(_ideaTable, {ideaCol: ideaStr},
        where: "$idCol = ?", whereArgs: [id]);
  }
}
