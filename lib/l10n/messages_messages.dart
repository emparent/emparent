// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a messages locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// ignore_for_file: unnecessary_brace_in_string_interps

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'messages';

  static m0(gender) => "${Intl.gender(gender, female: 'because she needed', other: 'because he needed')}";

  static m1(gender) => "${Intl.gender(gender, female: 'because you needed', other: 'because you needed')}";

  static m2(gender, name) => "${Intl.gender(gender, female: 'How did ${name} feel?', other: 'How did ${name} feel?')}";

  static m3(gender) => "${Intl.gender(gender, female: 'How did you feel?', other: 'How did you feel?')}";

  static m4(gender) => "${Intl.gender(gender, female: 'This is her own description', other: 'This is his own description')}";

  static m5(gender, name) => "${Intl.gender(gender, female: '${name} felt', other: '${name} felt')}";

  static m6(gender, emotionList, name) => "${Intl.gender(gender, female: '${name} felt ${emotionList}, because she needed', other: '${name} felt ${emotionList}, because he needed')}";

  static m7(gender, name) => "${Intl.gender(gender, female: '${name} needed', other: '${name} needed')}";

  static m8(gender) => "${Intl.gender(gender, female: 'You felt', other: 'You felt')}";

  static m9(gender, emotionList) => "${Intl.gender(gender, female: 'You felt ${emotionList}, because you needed', other: 'You felt ${emotionList}, because you needed')}";

  static m10(gender) => "${Intl.gender(gender, female: 'You needed', other: 'You needed')}";

  static m11(entryCount) => "${Intl.plural(entryCount, one: '${entryCount} item', few: '${entryCount} items', other: '${entryCount} items')}";

  static m12(listLength, itemsExceptLast, lastItem) => "${Intl.plural(listLength, zero: '', one: '${lastItem}', two: '${itemsExceptLast} and ${lastItem}', other: '${itemsExceptLast}, and ${lastItem}')}";

  static m13(itemName) => "Delete ${itemName}?";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "1. Try to uncover the child\'s **emotions and basic needs** in the situation" : MessageLookupByLibrary.simpleMessage("1. Try to uncover the child\'s **emotions and basic needs** in the situation"),
    "2. **Check your assumptions with your child** and hear out how they see it" : MessageLookupByLibrary.simpleMessage("2. **Check your assumptions with your child** and hear out how they see it"),
    "3. Share your **emotions and needs** with your child" : MessageLookupByLibrary.simpleMessage("3. Share your **emotions and needs** with your child"),
    "4. Make sure that your **child understood you** and didn\'t hear criticism" : MessageLookupByLibrary.simpleMessage("4. Make sure that your **child understood you** and didn\'t hear criticism"),
    "5. **Work together toward a solution** that meets everyone\'s goals" : MessageLookupByLibrary.simpleMessage("5. **Work together toward a solution** that meets everyone\'s goals"),
    "A reminder to record a situation" : MessageLookupByLibrary.simpleMessage("A reminder to record a situation"),
    "Add" : MessageLookupByLibrary.simpleMessage("Add"),
    "Add basic need" : MessageLookupByLibrary.simpleMessage("Add basic need"),
    "Add follow-up situation" : MessageLookupByLibrary.simpleMessage("Add follow-up situation"),
    "Add idea" : MessageLookupByLibrary.simpleMessage("Add idea"),
    "Add need" : MessageLookupByLibrary.simpleMessage("Add need"),
    "Add people" : MessageLookupByLibrary.simpleMessage("Add people"),
    "Add person" : MessageLookupByLibrary.simpleMessage("Add person"),
    "Add situation" : MessageLookupByLibrary.simpleMessage("Add situation"),
    "Back" : MessageLookupByLibrary.simpleMessage("Back"),
    "Basic need" : MessageLookupByLibrary.simpleMessage("Basic need"),
    "Belonging" : MessageLookupByLibrary.simpleMessage("Belonging"),
    "By gaining understanding, you\'ll find the right solution" : MessageLookupByLibrary.simpleMessage("By gaining understanding, you\'ll find the right solution"),
    "Cancel" : MessageLookupByLibrary.simpleMessage("Cancel"),
    "Community" : MessageLookupByLibrary.simpleMessage("Community"),
    "Could not open email app to export data with." : MessageLookupByLibrary.simpleMessage("Could not open email app to export data with."),
    "Daily notification" : MessageLookupByLibrary.simpleMessage("Daily notification"),
    "Delete" : MessageLookupByLibrary.simpleMessage("Delete"),
    "Delete topic" : MessageLookupByLibrary.simpleMessage("Delete topic"),
    "Deleting a person also deletes all of the entries associated with them, including their emotions and needs. If you\'re confident about deleting this person, please type their name in the field below." : MessageLookupByLibrary.simpleMessage("Deleting a person also deletes all of the entries associated with them, including their emotions and needs. If you\'re confident about deleting this person, please type their name in the field below."),
    "Describe a situation from " : MessageLookupByLibrary.simpleMessage("Describe a situation from "),
    "Done" : MessageLookupByLibrary.simpleMessage("Done"),
    "Edit" : MessageLookupByLibrary.simpleMessage("Edit"),
    "Emparent will ask you questions that will lead to a deeper understanding of the situation" : MessageLookupByLibrary.simpleMessage("Emparent will ask you questions that will lead to a deeper understanding of the situation"),
    "Emparent will help you solve parenting problems!" : MessageLookupByLibrary.simpleMessage("Emparent will help you solve parenting problems!"),
    "Export data" : MessageLookupByLibrary.simpleMessage("Export data"),
    "Feedback" : MessageLookupByLibrary.simpleMessage("Feedback"),
    "Freedom" : MessageLookupByLibrary.simpleMessage("Freedom"),
    "Harmony and beauty" : MessageLookupByLibrary.simpleMessage("Harmony and beauty"),
    "Hide resolved topics" : MessageLookupByLibrary.simpleMessage("Hide resolved topics"),
    "Idea for a solution" : MessageLookupByLibrary.simpleMessage("Idea for a solution"),
    "Knowledge and understanding" : MessageLookupByLibrary.simpleMessage("Knowledge and understanding"),
    "Manage people" : MessageLookupByLibrary.simpleMessage("Manage people"),
    "Mark as resolved" : MessageLookupByLibrary.simpleMessage("Mark as resolved"),
    "Mark as unresolved" : MessageLookupByLibrary.simpleMessage("Mark as unresolved"),
    "Name" : MessageLookupByLibrary.simpleMessage("Name"),
    "Name does not match" : MessageLookupByLibrary.simpleMessage("Name does not match"),
    "Next" : MessageLookupByLibrary.simpleMessage("Next"),
    "Not sure in this instance?" : MessageLookupByLibrary.simpleMessage("Not sure in this instance?"),
    "Participation" : MessageLookupByLibrary.simpleMessage("Participation"),
    "Personal notes (optional)" : MessageLookupByLibrary.simpleMessage("Personal notes (optional)"),
    "Physiological health" : MessageLookupByLibrary.simpleMessage("Physiological health"),
    "Please choose emotions first" : MessageLookupByLibrary.simpleMessage("Please choose emotions first"),
    "Record a situation so that you can solve it better next time!" : MessageLookupByLibrary.simpleMessage("Record a situation so that you can solve it better next time!"),
    "Resolved" : MessageLookupByLibrary.simpleMessage("Resolved"),
    "Resources" : MessageLookupByLibrary.simpleMessage("Resources"),
    "Respect" : MessageLookupByLibrary.simpleMessage("Respect"),
    "Safety / certainty" : MessageLookupByLibrary.simpleMessage("Safety / certainty"),
    "Self-actualization" : MessageLookupByLibrary.simpleMessage("Self-actualization"),
    "Show intro" : MessageLookupByLibrary.simpleMessage("Show intro"),
    "Show resolved topics" : MessageLookupByLibrary.simpleMessage("Show resolved topics"),
    "Skip intro" : MessageLookupByLibrary.simpleMessage("Skip intro"),
    "Talk with other parents" : MessageLookupByLibrary.simpleMessage("Talk with other parents"),
    "The first step toward a solution is understanding the problem" : MessageLookupByLibrary.simpleMessage("The first step toward a solution is understanding the problem"),
    "Time to record a situation!" : MessageLookupByLibrary.simpleMessage("Time to record a situation!"),
    "Title (optional)" : MessageLookupByLibrary.simpleMessage("Title (optional)"),
    "To avoid limiting your range of possible solutions, it\'s best to focus on basic human needs. There should be many different possible solutions to fulfil a human need and the need should not be tied to a specific person." : MessageLookupByLibrary.simpleMessage("To avoid limiting your range of possible solutions, it\'s best to focus on basic human needs. There should be many different possible solutions to fulfil a human need and the need should not be tied to a specific person."),
    "To describe the situation, you have to add the people involved" : MessageLookupByLibrary.simpleMessage("To describe the situation, you have to add the people involved"),
    "To meet everyone\'s needs, you could..." : MessageLookupByLibrary.simpleMessage("To meet everyone\'s needs, you could..."),
    "Try to think up as many possible solutions as you can!" : MessageLookupByLibrary.simpleMessage("Try to think up as many possible solutions as you can!"),
    "Unresolved" : MessageLookupByLibrary.simpleMessage("Unresolved"),
    "Untitled" : MessageLookupByLibrary.simpleMessage("Untitled"),
    "View general example" : MessageLookupByLibrary.simpleMessage("View general example"),
    "We\'ll guide you to a solution" : MessageLookupByLibrary.simpleMessage("We\'ll guide you to a solution"),
    "We\'ll help with understanding, but you\'ll get to full understanding and a fitting solution with your child afterward" : MessageLookupByLibrary.simpleMessage("We\'ll help with understanding, but you\'ll get to full understanding and a fitting solution with your child afterward"),
    "We\'ll help you find a solution, no matter the issue" : MessageLookupByLibrary.simpleMessage("We\'ll help you find a solution, no matter the issue"),
    "When you get the opportunity, it\'s time to apply this same process in practice." : MessageLookupByLibrary.simpleMessage("When you get the opportunity, it\'s time to apply this same process in practice."),
    "Who was involved?" : MessageLookupByLibrary.simpleMessage("Who was involved?"),
    "You" : MessageLookupByLibrary.simpleMessage("You"),
    "You have to choose at least one more person" : MessageLookupByLibrary.simpleMessage("You have to choose at least one more person"),
    "You\'ll find a solution through empathy!" : MessageLookupByLibrary.simpleMessage("You\'ll find a solution through empathy!"),
    "acceptance" : MessageLookupByLibrary.simpleMessage("acceptance"),
    "affection" : MessageLookupByLibrary.simpleMessage("affection"),
    "air" : MessageLookupByLibrary.simpleMessage("air"),
    "authenticity" : MessageLookupByLibrary.simpleMessage("authenticity"),
    "awareness" : MessageLookupByLibrary.simpleMessage("awareness"),
    "be known" : MessageLookupByLibrary.simpleMessage("be known"),
    "be seen" : MessageLookupByLibrary.simpleMessage("be seen"),
    "be understood" : MessageLookupByLibrary.simpleMessage("be understood"),
    "beauty" : MessageLookupByLibrary.simpleMessage("beauty"),
    "belonging" : MessageLookupByLibrary.simpleMessage("belonging"),
    "care" : MessageLookupByLibrary.simpleMessage("care"),
    "celebration of life" : MessageLookupByLibrary.simpleMessage("celebration of life"),
    "certainty" : MessageLookupByLibrary.simpleMessage("certainty"),
    "challenge" : MessageLookupByLibrary.simpleMessage("challenge"),
    "clarity" : MessageLookupByLibrary.simpleMessage("clarity"),
    "closeness" : MessageLookupByLibrary.simpleMessage("closeness"),
    "collaboration" : MessageLookupByLibrary.simpleMessage("collaboration"),
    "communication" : MessageLookupByLibrary.simpleMessage("communication"),
    "community" : MessageLookupByLibrary.simpleMessage("community"),
    "company" : MessageLookupByLibrary.simpleMessage("company"),
    "compassion" : MessageLookupByLibrary.simpleMessage("compassion"),
    "competence" : MessageLookupByLibrary.simpleMessage("competence"),
    "consciousness" : MessageLookupByLibrary.simpleMessage("consciousness"),
    "consideration" : MessageLookupByLibrary.simpleMessage("consideration"),
    "consistency" : MessageLookupByLibrary.simpleMessage("consistency"),
    "contribution" : MessageLookupByLibrary.simpleMessage("contribution"),
    "creativity" : MessageLookupByLibrary.simpleMessage("creativity"),
    "discovery" : MessageLookupByLibrary.simpleMessage("discovery"),
    "ease" : MessageLookupByLibrary.simpleMessage("ease"),
    "effectiveness" : MessageLookupByLibrary.simpleMessage("effectiveness"),
    "efficacy" : MessageLookupByLibrary.simpleMessage("efficacy"),
    "empathy" : MessageLookupByLibrary.simpleMessage("empathy"),
    "equality" : MessageLookupByLibrary.simpleMessage("equality"),
    "fairness" : MessageLookupByLibrary.simpleMessage("fairness"),
    "food" : MessageLookupByLibrary.simpleMessage("food"),
    "freedom" : MessageLookupByLibrary.simpleMessage("freedom"),
    "freedom of choice" : MessageLookupByLibrary.simpleMessage("freedom of choice"),
    "genderBecauseTheyNeeded" : m0,
    "genderBecauseYouNeeded" : m1,
    "genderHowDidTheyFeel" : m2,
    "genderHowDidYouFeel" : m3,
    "genderTheirOwnDescription" : m4,
    "genderTheyFelt" : m5,
    "genderTheyFeltBecauseTheyNeeded" : m6,
    "genderTheyNeeded" : m7,
    "genderYouFelt" : m8,
    "genderYouFeltBecauseYouNeeded" : m9,
    "genderYouNeeded" : m10,
    "growth" : MessageLookupByLibrary.simpleMessage("growth"),
    "harmony" : MessageLookupByLibrary.simpleMessage("harmony"),
    "he" : MessageLookupByLibrary.simpleMessage("he"),
    "honesty" : MessageLookupByLibrary.simpleMessage("honesty"),
    "hope" : MessageLookupByLibrary.simpleMessage("hope"),
    "https://emparent.gitlab.io/en/resources/" : MessageLookupByLibrary.simpleMessage("https://emparent.gitlab.io/en/resources/"),
    "https://parenting.stackexchange.com/" : MessageLookupByLibrary.simpleMessage("https://parenting.stackexchange.com/"),
    "https://www.youtube-nocookie.com/embed/TPifNZKb1og" : MessageLookupByLibrary.simpleMessage("https://www.youtube-nocookie.com/embed/TPifNZKb1og"),
    "humor" : MessageLookupByLibrary.simpleMessage("humor"),
    "inclusion" : MessageLookupByLibrary.simpleMessage("inclusion"),
    "independence" : MessageLookupByLibrary.simpleMessage("independence"),
    "inner peace" : MessageLookupByLibrary.simpleMessage("inner peace"),
    "inspiration" : MessageLookupByLibrary.simpleMessage("inspiration"),
    "integrity" : MessageLookupByLibrary.simpleMessage("integrity"),
    "intimacy" : MessageLookupByLibrary.simpleMessage("intimacy"),
    "joy" : MessageLookupByLibrary.simpleMessage("joy"),
    "learning" : MessageLookupByLibrary.simpleMessage("learning"),
    "love" : MessageLookupByLibrary.simpleMessage("love"),
    "mourning" : MessageLookupByLibrary.simpleMessage("mourning"),
    "movement / exercise" : MessageLookupByLibrary.simpleMessage("movement / exercise"),
    "order" : MessageLookupByLibrary.simpleMessage("order"),
    "participation" : MessageLookupByLibrary.simpleMessage("participation"),
    "partnership" : MessageLookupByLibrary.simpleMessage("partnership"),
    "physical health" : MessageLookupByLibrary.simpleMessage("physical health"),
    "physical safety" : MessageLookupByLibrary.simpleMessage("physical safety"),
    "plafulness" : MessageLookupByLibrary.simpleMessage("plafulness"),
    "pluralItems" : m11,
    "pluralList" : m12,
    "presence" : MessageLookupByLibrary.simpleMessage("presence"),
    "purpose" : MessageLookupByLibrary.simpleMessage("purpose"),
    "quiet" : MessageLookupByLibrary.simpleMessage("quiet"),
    "reciprocity" : MessageLookupByLibrary.simpleMessage("reciprocity"),
    "recognition" : MessageLookupByLibrary.simpleMessage("recognition"),
    "respect" : MessageLookupByLibrary.simpleMessage("respect"),
    "rest / sleep" : MessageLookupByLibrary.simpleMessage("rest / sleep"),
    "safety" : MessageLookupByLibrary.simpleMessage("safety"),
    "security" : MessageLookupByLibrary.simpleMessage("security"),
    "self-expression" : MessageLookupByLibrary.simpleMessage("self-expression"),
    "sexual expression" : MessageLookupByLibrary.simpleMessage("sexual expression"),
    "she" : MessageLookupByLibrary.simpleMessage("she"),
    "shelter" : MessageLookupByLibrary.simpleMessage("shelter"),
    "space" : MessageLookupByLibrary.simpleMessage("space"),
    "spontaneity" : MessageLookupByLibrary.simpleMessage("spontaneity"),
    "stability" : MessageLookupByLibrary.simpleMessage("stability"),
    "stimulation" : MessageLookupByLibrary.simpleMessage("stimulation"),
    "support" : MessageLookupByLibrary.simpleMessage("support"),
    "textSureAboutDelete" : m13,
    "to know" : MessageLookupByLibrary.simpleMessage("to know"),
    "to matter" : MessageLookupByLibrary.simpleMessage("to matter"),
    "to understand" : MessageLookupByLibrary.simpleMessage("to understand"),
    "touch" : MessageLookupByLibrary.simpleMessage("touch"),
    "trust" : MessageLookupByLibrary.simpleMessage("trust"),
    "understanding" : MessageLookupByLibrary.simpleMessage("understanding"),
    "warmth" : MessageLookupByLibrary.simpleMessage("warmth"),
    "water" : MessageLookupByLibrary.simpleMessage("water")
  };
}
