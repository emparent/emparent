// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a cs locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// ignore_for_file: unnecessary_brace_in_string_interps

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'cs';

  static m0(gender) => "${Intl.gender(gender, female: 'protože potřebovala', other: 'protože potřeboval')}";

  static m1(gender) => "${Intl.gender(gender, female: 'protože jste potřebovala', other: 'protože jste potřeboval')}";

  static m2(gender, name) => "${Intl.gender(gender, female: 'Jak se cítila ${name}?', other: 'Jak se cítil ${name}?')}";

  static m3(gender) => "${Intl.gender(gender, female: 'Jak jste se cítila?', other: 'Jak jste se cítil?')}";

  static m4(gender) => "${Intl.gender(gender, female: 'Jde o její vlastní popis', other: 'Jde o jeho vlastní popis')}";

  static m5(gender, name) => "${Intl.gender(gender, female: '${name} se cítil', other: '${name} se cítil')}";

  static m6(gender, emotionList, name) => "${Intl.gender(gender, female: '${name} se cítila ${emotionList}, protože potřebovala', other: '${name} se cítil ${emotionList}, protože potřeboval')}";

  static m7(gender, name) => "${Intl.gender(gender, female: '${name} potřebovala', other: '${name} potřeboval')}";

  static m8(gender) => "${Intl.gender(gender, female: 'Cítila jste se', other: 'Cítil jste se')}";

  static m9(gender, emotionList) => "${Intl.gender(gender, female: 'Cítila jste se ${emotionList}, protože jste potřebovala', other: 'Cítil jste se ${emotionList}, protože jste potřeboval')}";

  static m10(gender) => "${Intl.gender(gender, female: 'Potřebovala jste', other: 'Potřeboval jste')}";

  static m11(entryCount) => "${Intl.plural(entryCount, one: '${entryCount} položka', few: '${entryCount} položky', other: '${entryCount} položek')}";

  static m12(listLength, itemsExceptLast, lastItem) => "${Intl.plural(listLength, zero: '', one: '${lastItem}', two: '${itemsExceptLast} a ${lastItem}', other: '${itemsExceptLast} a ${lastItem}')}";

  static m13(itemName) => "Odstranit ${itemName}?";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "1. Try to uncover the child\'s **emotions and basic needs** in the situation" : MessageLookupByLibrary.simpleMessage("1. Pokuste se v situaci odhalit **emoce a základní potřeby** dítěte"),
    "2. **Check your assumptions with your child** and hear out how they see it" : MessageLookupByLibrary.simpleMessage("2. **Ověřte si své domněnky s dítětem** a vyslechněte si, jak to vidí"),
    "3. Share your **emotions and needs** with your child" : MessageLookupByLibrary.simpleMessage("3. Sdělte své **emoce a potřeby** dítěti"),
    "4. Make sure that your **child understood you** and didn\'t hear criticism" : MessageLookupByLibrary.simpleMessage("4. Ujistěte se, že Vám **dítě porozumělo** a neslyšelo kritiku"),
    "5. **Work together toward a solution** that meets everyone\'s goals" : MessageLookupByLibrary.simpleMessage("5. **5. **Společně se dopracujte k řešení**, které pokryje všechny vaše potřeby"),
    "A reminder to record a situation" : MessageLookupByLibrary.simpleMessage("Připomínka o zaznamenání situace"),
    "Add" : MessageLookupByLibrary.simpleMessage("Přidat"),
    "Add basic need" : MessageLookupByLibrary.simpleMessage("Přidat základní potřebu"),
    "Add follow-up situation" : MessageLookupByLibrary.simpleMessage("Přidat navazující potřebu"),
    "Add idea" : MessageLookupByLibrary.simpleMessage("Přidat nápad"),
    "Add need" : MessageLookupByLibrary.simpleMessage("Přidat potřebu"),
    "Add people" : MessageLookupByLibrary.simpleMessage("Přidat osoby"),
    "Add person" : MessageLookupByLibrary.simpleMessage("Přidat osobu"),
    "Add situation" : MessageLookupByLibrary.simpleMessage("Přidat situaci"),
    "Back" : MessageLookupByLibrary.simpleMessage("Zpět"),
    "Basic need" : MessageLookupByLibrary.simpleMessage("Základní potřeba"),
    "Belonging" : MessageLookupByLibrary.simpleMessage("Sounáležitost"),
    "By gaining understanding, you\'ll find the right solution" : MessageLookupByLibrary.simpleMessage("Skrze porozumění najdete vhodné řešení"),
    "Cancel" : MessageLookupByLibrary.simpleMessage("Zrušit"),
    "Community" : MessageLookupByLibrary.simpleMessage("Komunita (v AJ)"),
    "Could not open email app to export data with." : MessageLookupByLibrary.simpleMessage("E-mailový klient nelze otevřít."),
    "Daily notification" : MessageLookupByLibrary.simpleMessage("Každodenní připomínka"),
    "Delete" : MessageLookupByLibrary.simpleMessage("Smazat"),
    "Delete topic" : MessageLookupByLibrary.simpleMessage("Smazat téma"),
    "Deleting a person also deletes all of the entries associated with them, including their emotions and needs. If you\'re confident about deleting this person, please type their name in the field below." : MessageLookupByLibrary.simpleMessage("Odstraněním člověka také odstraníte všechny záznamy týkající se člověka, včetně jeho emocí a potřeb. Pokud jste si jisti odstraněním, napište jméno člověka do pole níže."),
    "Describe a situation from " : MessageLookupByLibrary.simpleMessage("Popište situaci z "),
    "Done" : MessageLookupByLibrary.simpleMessage("Hotovo"),
    "Edit" : MessageLookupByLibrary.simpleMessage("Upravit"),
    "Emparent will ask you questions that will lead to a deeper understanding of the situation" : MessageLookupByLibrary.simpleMessage("Emparent Vám položí otázky, které povedou k hlubšímu pochopení situace"),
    "Emparent will help you solve parenting problems!" : MessageLookupByLibrary.simpleMessage("Emparent Vám pomůže řešit rodičovské problémy!"),
    "Export data" : MessageLookupByLibrary.simpleMessage("Exportovat data"),
    "Feedback" : MessageLookupByLibrary.simpleMessage("Zpětná vazba"),
    "Freedom" : MessageLookupByLibrary.simpleMessage("Svobodu"),
    "Harmony and beauty" : MessageLookupByLibrary.simpleMessage("Harmonii a krásu"),
    "Hide resolved topics" : MessageLookupByLibrary.simpleMessage("Skrýt vyřešená témata"),
    "Idea for a solution" : MessageLookupByLibrary.simpleMessage("Nápad na řešení"),
    "Knowledge and understanding" : MessageLookupByLibrary.simpleMessage("Znalost a porozumění"),
    "Manage people" : MessageLookupByLibrary.simpleMessage("Spravovat osoby"),
    "Mark as resolved" : MessageLookupByLibrary.simpleMessage("Označit jako vyřešené"),
    "Mark as unresolved" : MessageLookupByLibrary.simpleMessage("Označit jako nevyřešené"),
    "Name" : MessageLookupByLibrary.simpleMessage("Jméno"),
    "Name does not match" : MessageLookupByLibrary.simpleMessage("Neodpovídá uvedenému jménu"),
    "Next" : MessageLookupByLibrary.simpleMessage("Dále"),
    "Not sure in this instance?" : MessageLookupByLibrary.simpleMessage("Nejste si v tomto případě jistí?"),
    "Participation" : MessageLookupByLibrary.simpleMessage("Účast"),
    "Personal notes (optional)" : MessageLookupByLibrary.simpleMessage("Vlastní poznámky (nepovinné)"),
    "Physiological health" : MessageLookupByLibrary.simpleMessage("Fyziologické zdraví"),
    "Please choose emotions first" : MessageLookupByLibrary.simpleMessage("Vyberte, prosím, nejprve emoce"),
    "Record a situation so that you can solve it better next time!" : MessageLookupByLibrary.simpleMessage("Zaznamenejte situaci, abyste ji mohl/a příště vyřešit lépe!"),
    "Resolved" : MessageLookupByLibrary.simpleMessage("Vyřešeno"),
    "Resources" : MessageLookupByLibrary.simpleMessage("Zdroje"),
    "Respect" : MessageLookupByLibrary.simpleMessage("Respekt"),
    "Safety / certainty" : MessageLookupByLibrary.simpleMessage("Bezpečí / jistotu"),
    "Self-actualization" : MessageLookupByLibrary.simpleMessage("Seberealizaci"),
    "Show intro" : MessageLookupByLibrary.simpleMessage("Zobrazit intro"),
    "Show resolved topics" : MessageLookupByLibrary.simpleMessage("Zobrazit vyřešená témata"),
    "Skip intro" : MessageLookupByLibrary.simpleMessage("Přeskočit intro"),
    "Talk with other parents" : MessageLookupByLibrary.simpleMessage("Poradit se s rodiči (v AJ)"),
    "The first step toward a solution is understanding the problem" : MessageLookupByLibrary.simpleMessage("První krok k řešení je porozumění problému"),
    "Time to record a situation!" : MessageLookupByLibrary.simpleMessage("Čas zaznamenat situaci!"),
    "Title (optional)" : MessageLookupByLibrary.simpleMessage("Název (nepovinný)"),
    "To avoid limiting your range of possible solutions, it\'s best to focus on basic human needs. There should be many different possible solutions to fulfil a human need and the need should not be tied to a specific person." : MessageLookupByLibrary.simpleMessage("Abyste si zbytečně neomezil/a škálu možných řešení, je vhodné se zaměřit na obecné lidské potřeby. Ty se vyznačují tím, že jich lze dosáhnout různými způsoby a nejsou vázané na konkrétní osoby."),
    "To describe the situation, you have to add the people involved" : MessageLookupByLibrary.simpleMessage("Abyste mohl/a popisovat situace, musíte přidat zapojené osoby"),
    "To meet everyone\'s needs, you could..." : MessageLookupByLibrary.simpleMessage("Aby se pokryly potřeby všech, mohli byste..."),
    "Try to think up as many possible solutions as you can!" : MessageLookupByLibrary.simpleMessage("Pojďte vymyslet co nejvíce možných řešení!"),
    "Unresolved" : MessageLookupByLibrary.simpleMessage("Nevyřešeno"),
    "Untitled" : MessageLookupByLibrary.simpleMessage("Bez názvu"),
    "View general example" : MessageLookupByLibrary.simpleMessage("Shlédnout obecný příklad"),
    "We\'ll guide you to a solution" : MessageLookupByLibrary.simpleMessage("Navedeme Vás k řešení"),
    "We\'ll help with understanding, but you\'ll get to full understanding and a fitting solution with your child afterward" : MessageLookupByLibrary.simpleMessage("My s porozuměním pomůžeme, ale k úplnému porozumění a řešení se doberete až s dítětem"),
    "We\'ll help you find a solution, no matter the issue" : MessageLookupByLibrary.simpleMessage("Pomůžeme vám najít řešení, ať už se jedná o cokoliv"),
    "When you get the opportunity, it\'s time to apply this same process in practice." : MessageLookupByLibrary.simpleMessage("Až nastane příležitost, je na čase uvést stejný postup v praxi."),
    "Who was involved?" : MessageLookupByLibrary.simpleMessage("Kdo všechno se účastnil?"),
    "You" : MessageLookupByLibrary.simpleMessage("Vy"),
    "You have to choose at least one more person" : MessageLookupByLibrary.simpleMessage("Musíte vybrat alespoň jednoho dalšího člověka"),
    "You\'ll find a solution through empathy!" : MessageLookupByLibrary.simpleMessage("Pomocí empatie dojdete k řešení!"),
    "acceptance" : MessageLookupByLibrary.simpleMessage("přijetí"),
    "affection" : MessageLookupByLibrary.simpleMessage("náklonnost"),
    "air" : MessageLookupByLibrary.simpleMessage("vzduch"),
    "authenticity" : MessageLookupByLibrary.simpleMessage("autentičnost"),
    "awareness" : MessageLookupByLibrary.simpleMessage("povědomí"),
    "be known" : MessageLookupByLibrary.simpleMessage("být znán/a"),
    "be seen" : MessageLookupByLibrary.simpleMessage("být viděn/a"),
    "be understood" : MessageLookupByLibrary.simpleMessage("být pochopen/a"),
    "beauty" : MessageLookupByLibrary.simpleMessage("krásu"),
    "belonging" : MessageLookupByLibrary.simpleMessage("sounáležitost"),
    "care" : MessageLookupByLibrary.simpleMessage("péči"),
    "celebration of life" : MessageLookupByLibrary.simpleMessage("oslavu života"),
    "certainty" : MessageLookupByLibrary.simpleMessage("jistotu"),
    "challenge" : MessageLookupByLibrary.simpleMessage("výzvu"),
    "clarity" : MessageLookupByLibrary.simpleMessage("srozumitelnost"),
    "closeness" : MessageLookupByLibrary.simpleMessage("blízkost"),
    "collaboration" : MessageLookupByLibrary.simpleMessage("spolupráci"),
    "communication" : MessageLookupByLibrary.simpleMessage("komunikaci"),
    "community" : MessageLookupByLibrary.simpleMessage("společenství"),
    "company" : MessageLookupByLibrary.simpleMessage("společnost"),
    "compassion" : MessageLookupByLibrary.simpleMessage("soucit"),
    "competence" : MessageLookupByLibrary.simpleMessage("kompetenci"),
    "consciousness" : MessageLookupByLibrary.simpleMessage("uvědomělost"),
    "consideration" : MessageLookupByLibrary.simpleMessage("ohleduplnost"),
    "consistency" : MessageLookupByLibrary.simpleMessage("konzistenci"),
    "contribution" : MessageLookupByLibrary.simpleMessage("podíl"),
    "creativity" : MessageLookupByLibrary.simpleMessage("tvořivost"),
    "discovery" : MessageLookupByLibrary.simpleMessage("objevování"),
    "ease" : MessageLookupByLibrary.simpleMessage("jednoduchost"),
    "effectiveness" : MessageLookupByLibrary.simpleMessage("efektivnost"),
    "efficacy" : MessageLookupByLibrary.simpleMessage("účinnost"),
    "empathy" : MessageLookupByLibrary.simpleMessage("empatii"),
    "equality" : MessageLookupByLibrary.simpleMessage("rovnost"),
    "fairness" : MessageLookupByLibrary.simpleMessage("férovost"),
    "food" : MessageLookupByLibrary.simpleMessage("jídlo"),
    "freedom" : MessageLookupByLibrary.simpleMessage("svobodu"),
    "freedom of choice" : MessageLookupByLibrary.simpleMessage("svobodnou volbu"),
    "genderBecauseTheyNeeded" : m0,
    "genderBecauseYouNeeded" : m1,
    "genderHowDidTheyFeel" : m2,
    "genderHowDidYouFeel" : m3,
    "genderTheirOwnDescription" : m4,
    "genderTheyFelt" : m5,
    "genderTheyFeltBecauseTheyNeeded" : m6,
    "genderTheyNeeded" : m7,
    "genderYouFelt" : m8,
    "genderYouFeltBecauseYouNeeded" : m9,
    "genderYouNeeded" : m10,
    "growth" : MessageLookupByLibrary.simpleMessage("růst"),
    "harmony" : MessageLookupByLibrary.simpleMessage("harmonii"),
    "he" : MessageLookupByLibrary.simpleMessage("on"),
    "honesty" : MessageLookupByLibrary.simpleMessage("upřímnost"),
    "hope" : MessageLookupByLibrary.simpleMessage("naději"),
    "https://emparent.gitlab.io/en/resources/" : MessageLookupByLibrary.simpleMessage("https://emparent.gitlab.io/cz/resources/"),
    "https://parenting.stackexchange.com/" : MessageLookupByLibrary.simpleMessage("https://parenting.stackexchange.com/"),
    "https://www.youtube-nocookie.com/embed/TPifNZKb1og" : MessageLookupByLibrary.simpleMessage("https://www.youtube-nocookie.com/embed/TPifNZKb1og?hl=cs&cc_lang_pref=cs&cc_load_policy=1"),
    "humor" : MessageLookupByLibrary.simpleMessage("humor"),
    "inclusion" : MessageLookupByLibrary.simpleMessage("začlenění"),
    "independence" : MessageLookupByLibrary.simpleMessage("nezávislost"),
    "inner peace" : MessageLookupByLibrary.simpleMessage("duševní klid"),
    "inspiration" : MessageLookupByLibrary.simpleMessage("inspiraci"),
    "integrity" : MessageLookupByLibrary.simpleMessage("integritu"),
    "intimacy" : MessageLookupByLibrary.simpleMessage("intimitu"),
    "joy" : MessageLookupByLibrary.simpleMessage("radost"),
    "learning" : MessageLookupByLibrary.simpleMessage("učení se"),
    "love" : MessageLookupByLibrary.simpleMessage("lásku"),
    "mourning" : MessageLookupByLibrary.simpleMessage("truchlení"),
    "movement / exercise" : MessageLookupByLibrary.simpleMessage("cvičení / pohyb"),
    "order" : MessageLookupByLibrary.simpleMessage("pořádek"),
    "participation" : MessageLookupByLibrary.simpleMessage("účast"),
    "partnership" : MessageLookupByLibrary.simpleMessage("partnerství"),
    "physical health" : MessageLookupByLibrary.simpleMessage("fyziologické zdraví"),
    "physical safety" : MessageLookupByLibrary.simpleMessage("fyzické bezpečí"),
    "plafulness" : MessageLookupByLibrary.simpleMessage("hravost"),
    "pluralItems" : m11,
    "pluralList" : m12,
    "presence" : MessageLookupByLibrary.simpleMessage("přítomnost"),
    "purpose" : MessageLookupByLibrary.simpleMessage("smysl"),
    "quiet" : MessageLookupByLibrary.simpleMessage("klid"),
    "reciprocity" : MessageLookupByLibrary.simpleMessage("vzájemnost"),
    "recognition" : MessageLookupByLibrary.simpleMessage("uznání"),
    "respect" : MessageLookupByLibrary.simpleMessage("respekt"),
    "rest / sleep" : MessageLookupByLibrary.simpleMessage("odpočinek / spánek"),
    "safety" : MessageLookupByLibrary.simpleMessage("bezpečí"),
    "security" : MessageLookupByLibrary.simpleMessage("zabezpečení"),
    "self-expression" : MessageLookupByLibrary.simpleMessage("sebevyjádření"),
    "sexual expression" : MessageLookupByLibrary.simpleMessage("sexuální vyjádření"),
    "she" : MessageLookupByLibrary.simpleMessage("ona"),
    "shelter" : MessageLookupByLibrary.simpleMessage("přístřeší"),
    "space" : MessageLookupByLibrary.simpleMessage("prostor"),
    "spontaneity" : MessageLookupByLibrary.simpleMessage("spontánnost"),
    "stability" : MessageLookupByLibrary.simpleMessage("stabilitu"),
    "stimulation" : MessageLookupByLibrary.simpleMessage("stimulaci"),
    "support" : MessageLookupByLibrary.simpleMessage("podporu"),
    "textSureAboutDelete" : m13,
    "to know" : MessageLookupByLibrary.simpleMessage("znát"),
    "to matter" : MessageLookupByLibrary.simpleMessage("význam"),
    "to understand" : MessageLookupByLibrary.simpleMessage("pochopit"),
    "touch" : MessageLookupByLibrary.simpleMessage("dotek"),
    "trust" : MessageLookupByLibrary.simpleMessage("důvěru"),
    "understanding" : MessageLookupByLibrary.simpleMessage("porozumění"),
    "warmth" : MessageLookupByLibrary.simpleMessage("teplo"),
    "water" : MessageLookupByLibrary.simpleMessage("vodu")
  };
}
