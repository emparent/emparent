// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'messages_all.dart';

class L10n {
  static const SUPPORTED_LOCALES = [
    const Locale('en'),
    const Locale('cs'),
  ];

  static Future<L10n> load(Locale locale) {
    final String name =
        locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return L10n();
    });
  }

  static L10n of(BuildContext context) {
    return Localizations.of<L10n>(context, L10n);
  }

  String get btnBack => Intl.message("Back");
  String get btnNext => Intl.message("Next");
  String get btnDone => Intl.message("Done");
  String get btnCancel => Intl.message("Cancel");
  String get btnAdd => Intl.message("Add");
  String get btnAddNeed => Intl.message("Add need");
  String get btnFollowUpSituation => Intl.message('Add follow-up situation');
  String get btnResolve => Intl.message("Mark as resolved");
  String get btnUnresolve => Intl.message("Mark as unresolved");
  String get btnHideResolved => Intl.message("Hide resolved topics");
  String get btnShowResolved => Intl.message("Show resolved topics");

  String get btnAddSituation => Intl.message("Add situation");
  String get btnManagePeople => Intl.message("Manage people");
  String get btnResources => Intl.message("Resources");
  String get btnCommunity => Intl.message("Community");
  String get btnShowIntro => Intl.message("Show intro");
  String get btnExportData => Intl.message("Export data");
  String get btnFeedback => Intl.message("Feedback");
  String get btnSkipIntro => Intl.message("Skip intro");
  String get btnAddPeople => Intl.message("Add people");
  String get btnAddPerson => Intl.message("Add person");
  String get btnAddIdea => Intl.message("Add idea");
  String get btnShowVideoExample => Intl.message("View general example");
  String get btnTalkWithOthers => Intl.message("Talk with other parents");
  String get btnDeleteTopic => Intl.message('Delete topic');
  String get btnEdit => Intl.message('Edit');
  String get btnDelete => Intl.message('Delete');

  String get urlCommunity =>
      Intl.message("https://parenting.stackexchange.com/");
  String get urlResources =>
      Intl.message("https://emparent.gitlab.io/en/resources/");
  String get urlVideoExample =>
      Intl.message("https://www.youtube-nocookie.com/embed/TPifNZKb1og");

  String pluralItems(int entryCount) => Intl.plural(entryCount,
      one: '$entryCount item',
      few: '$entryCount items',
      other: '$entryCount items',
      name: "pluralItems",
      args: [entryCount]);

  String getPluralList(List<String> list) {
    var lastItem = list.length > 0 ? list.last : "";
    var itemsExceptLast =
        (list.length > 1) ? list.sublist(0, list.length - 1).join(", ") : "";
    var listLength = list.length;
    return pluralList(listLength, itemsExceptLast, lastItem);
  }

  String pluralList(int listLength, String itemsExceptLast, String lastItem) =>
      Intl.plural(listLength,
          zero: "",
          one: "$lastItem",
          two: "$itemsExceptLast and $lastItem",
          other: "$itemsExceptLast, and $lastItem",
          name: "pluralList",
          args: [listLength, itemsExceptLast, lastItem]);

  String genderHowDidYouFeel(String gender) => Intl.gender(gender,
      female: "How did you feel?",
      other: "How did you feel?",
      name: "genderHowDidYouFeel",
      args: [gender]);
  String genderHowDidTheyFeel(String gender, String name) => Intl.gender(gender,
      female: "How did $name feel?",
      other: "How did $name feel?",
      name: "genderHowDidTheyFeel",
      args: [gender, name]);
  String genderTheirOwnDescription(String gender) => Intl.gender(gender,
      female: "This is her own description",
      other: "This is his own description",
      name: "genderTheirOwnDescription",
      args: [gender]);
  String genderYouFeltBecauseYouNeeded(String gender, String emotionList) =>
      Intl.gender(gender,
          female: "You felt $emotionList, because you needed",
          other: "You felt $emotionList, because you needed",
          name: "genderYouFeltBecauseYouNeeded",
          args: [gender, emotionList]);
  String genderTheyFeltBecauseTheyNeeded(
          String gender, String emotionList, String name) =>
      Intl.gender(gender,
          female: "$name felt $emotionList, because she needed",
          other: "$name felt $emotionList, because he needed",
          name: "genderTheyFeltBecauseTheyNeeded",
          args: [gender, emotionList, name]);
  String genderYouNeeded(String gender) => Intl.gender(gender,
      female: "You needed",
      other: "You needed",
      name: "genderYouNeeded",
      args: [gender]);
  String genderTheyNeeded(String gender, String name) => Intl.gender(gender,
      name: "genderTheyNeeded",
      female: "$name needed",
      other: "$name needed",
      args: [gender, name]);
  String genderYouFelt(String gender) => Intl.gender(gender,
      female: "You felt",
      other: "You felt",
      name: "genderYouFelt",
      args: [gender]);
  String genderTheyFelt(String gender, String name) => Intl.gender(gender,
      female: "$name felt",
      other: "$name felt",
      name: "genderTheyFelt",
      args: [gender, name]);
  String genderBecauseYouNeeded(String gender) => Intl.gender(gender,
      female: "because you needed",
      other: "because you needed",
      name: "genderBecauseYouNeeded",
      args: [gender]);
  String genderBecauseTheyNeeded(String gender) => Intl.gender(gender,
      female: "because she needed",
      other: "because he needed",
      name: "genderBecauseTheyNeeded",
      args: [gender]);

  String get textYou => Intl.message("You");

  String get textName => Intl.message('Name');
  String get textHe => Intl.message('he');
  String get textShe => Intl.message('she');
  String get textManagePeople => Intl.message('Manage people');

  String get textResolved => Intl.message("Resolved");
  String get textUnresolved => Intl.message("Unresolved");

  String get textUntitled => Intl.message("Untitled");
  String get textIntro1Heading =>
      Intl.message("Emparent will help you solve parenting problems!");
  String get textIntro1Content =>
      Intl.message("We'll help you find a solution, no matter the issue");
  String get textIntro2Heading => Intl.message(
      "The first step toward a solution is understanding the problem");
  String get textIntro2Content => Intl.message(
      "Emparent will ask you questions that will lead to a deeper understanding of the situation");
  String get textIntro3Heading =>
      Intl.message("By gaining understanding, you'll find the right solution");
  String get textIntro3Content => Intl.message(
      "We'll help with understanding, but you'll get to full understanding and a fitting solution with your child afterward");

  String get textDescribeSituationFrom =>
      Intl.message('Describe a situation from ');
  String get textWhoWasInvolved => Intl.message('Who was involved?');

  String get textTitleOptional => Intl.message('Title (optional)');
  String get textNotesOptional => Intl.message('Personal notes (optional)');
  String get textHaveToAddInvoled => Intl.message(
      "To describe the situation, you have to add the people involved");
  String get textHaveToAddOneMoreInvolved =>
      Intl.message("You have to choose at least one more person");

  String get textHaveToChooseEmotions =>
      Intl.message("Please choose emotions first");

  String get textAddBasicNeed => Intl.message("Add basic need");
  String get textBasicNeed => Intl.message('Basic need');

  String get textFocusOnBasicHumanNeeds => Intl.message(
      "To avoid limiting your range of possible solutions, it's best to focus on basic human needs. There should be many different possible solutions to fulfil a human need and the need should not be tied to a specific person.");

  String get textThinkUpMultipleSolutions =>
      Intl.message('Try to think up as many possible solutions as you can!');
  String get textToMeetAllNeeds =>
      Intl.message("To meet everyone's needs, you could...");

  String get textSolutionIdea => Intl.message("Idea for a solution");

  String get textUseEmpathyToFindSolution =>
      Intl.message("You'll find a solution through empathy!");
  String get textApplyInPractice => Intl.message(
      "When you get the opportunity, it's time to apply this same process in practice.");
  String get textGuide1DiscoverEmotionsNeeds => Intl.message(
      "1. Try to uncover the child's **emotions and basic needs** in the situation");
  String get textGuide2CheckEmotionsNeeds => Intl.message(
      "2. **Check your assumptions with your child** and hear out how they see it");
  String get textGuide3ShareEmotionsNeeds =>
      Intl.message("3. Share your **emotions and needs** with your child");
  String get textGuide4ConfirmUnderstanding => Intl.message(
      "4. Make sure that your **child understood you** and didn't hear criticism");
  String get textGuide5WorkTowardSolution => Intl.message(
      "5. **Work together toward a solution** that meets everyone's goals");
  String get textNotSureInThisCase =>
      Intl.message("Not sure in this instance?");

  String get textDailyReminder => Intl.message("Daily notification");
  String get textReminderToRecordSituation =>
      Intl.message("A reminder to record a situation");
  String get textTimeToRecordSituation =>
      Intl.message("Time to record a situation!");
  String get textWellGuideYouToSolution =>
      Intl.message('We\'ll guide you to a solution');

  String textSureAboutDelete(String itemName) =>
      Intl.message("Delete $itemName?",
          name: "textSureAboutDelete", args: [itemName]);
  String get textDeletePersonMessage => Intl.message(
      "Deleting a person also deletes all of the entries associated with them, including their emotions and needs. If you're confident about deleting this person, please type their name in the field below.");
  String get textDoesNotMatchName => Intl.message("Name does not match");
  String get textCouldNotOpenEmail => Intl.message("Could not open email app to export data with.");

  String get textRecordSituation => Intl.message("Record a situation so that you can solve it better next time!");

  // NEEDS

  String get needPhysiology => Intl.message("Physiological health");
  String get needSafety => Intl.message("Safety / certainty");
  String get needFreedom => Intl.message("Freedom");
  String get needBelonging => Intl.message("Belonging");
  String get needRespect => Intl.message("Respect");
  String get needHarmony => Intl.message("Harmony and beauty");
  String get needUnderstanding => Intl.message("Knowledge and understanding");
  String get needSelfActualization => Intl.message("Self-actualization");
  String get needParticipation => Intl.message("Participation");

  String get needPhysiologyAir => Intl.message("air");
  String get needPhysiologyFood => Intl.message("food");
  String get needPhysiologyExercise => Intl.message("movement / exercise");
  String get needPhysiologyRest => Intl.message("rest / sleep");
  String get needPhysiologyHeallth => Intl.message("physical health");
  String get needPhysiologySex => Intl.message("sexual expression");
  String get needPhysiologySafety => Intl.message("physical safety");
  String get needPhysiologyHome => Intl.message("shelter");
  String get needPhysiologyTouch => Intl.message("touch");
  String get needPhysiologyWater => Intl.message("water");

  String get needSafetyPeace => Intl.message("inner peace");
  String get needSafetySafety => Intl.message("safety");
  String get needSafetySecurity => Intl.message("security");
  String get needSafetyCertainty => Intl.message("certainty");
  String get needSafetyStability => Intl.message("stability");
  String get needSafetySupport => Intl.message("support");

  String get needFreedomChoice => Intl.message("freedom of choice");
  String get needFreedomFreedom => Intl.message("freedom");
  String get needFreedomSpace => Intl.message("space");
  String get needFreedomSpontaneity => Intl.message("spontaneity");
  String get needFreedomIndependence => Intl.message("independence");

  String get needBelongingAcceptance => Intl.message("acceptance");
  String get needBelongingAffection => Intl.message("affection");
  String get needBelongingBelonging => Intl.message("belonging");
  String get needBelongingCollaboration => Intl.message("collaboration");
  String get needBelongingCommunication => Intl.message("communication");
  String get needBelongingCommunity => Intl.message("community");
  String get needBelongingCloseness => Intl.message("closeness");
  String get needBelongingCompany => Intl.message("company");
  String get needBelongingPartnership => Intl.message("partnership");
  String get needBelongingCompassion => Intl.message("compassion");
  String get needBelongingConsideration => Intl.message("consideration");
  String get needBelongingEmpathy => Intl.message("empathy");
  String get needBelongingInclusion => Intl.message("inclusion");
  String get needBelongingIntimacy => Intl.message("intimacy");
  String get needBelongingLove => Intl.message("love");
  String get needBelongingReciprocity => Intl.message("reciprocity");
  String get needBelongingCare => Intl.message("care");
  String get needBelongingWarmth => Intl.message("warmth");

  String get needRespectRecognition => Intl.message("recognition");
  String get needRespectCompetence => Intl.message("competence");
  String get needRespectRespect => Intl.message("respect");
  String get needRespectKnown => Intl.message("be known");
  String get needRespectSeen => Intl.message("be seen");
  String get needRespectUnderstood => Intl.message("be understood");
  String get needRespectTrust => Intl.message("trust");

  String get needHarmonyFairness => Intl.message("fairness");
  String get needHarmonyBeauty => Intl.message("beauty");
  String get needHarmonyConsistency => Intl.message("consistency");
  String get needHarmonyEquality => Intl.message("equality");
  String get needHarmonyHarmony => Intl.message("harmony");
  String get needHarmonyOrder => Intl.message("order");
  String get needHarmonyQuiet => Intl.message("quiet");

  String get needUnderstandingAuthenticity => Intl.message("authenticity");
  String get needUnderstandingHonesty => Intl.message("honesty");
  String get needUnderstandingAwareness => Intl.message("awareness");
  String get needUnderstandingClarity => Intl.message("clarity");
  String get needUnderstandingDiscovery => Intl.message("discovery");
  String get needUnderstandingIntegrity => Intl.message("integrity");
  String get needUnderstandingLearning => Intl.message("learning");
  String get needUnderstandingPresence => Intl.message("presence");
  String get needUnderstandingUnderstand => Intl.message("to understand");
  String get needUnderstandingKnow => Intl.message("to know");

  String get needActualizationLifeCelebration =>
      Intl.message("celebration of life");
  String get needActualizationChallenge => Intl.message("challenge");
  String get needActualizationConsciousness => Intl.message("consciousness");
  String get needActualizationCreativity => Intl.message("creativity");
  String get needActualizationEase => Intl.message("ease");
  String get needActualizationGrowth => Intl.message("growth");
  String get needActualizationHope => Intl.message("hope");
  String get needActualizationInspiration => Intl.message("inspiration");
  String get needActualizationJoy => Intl.message("joy");
  String get needActualizationHumor => Intl.message("humor");
  String get needActualizationMourning => Intl.message("mourning");
  String get needActualizationSelfExpression => Intl.message("self-expression");
  String get needActualizationPlayfulness => Intl.message("plafulness");
  String get needActualizationStimulation => Intl.message("stimulation");

  String get needParticipationContribution => Intl.message("contribution");
  String get needParticipationEfficacy => Intl.message("efficacy");
  String get needParticipationEffectiveness => Intl.message("effectiveness");
  String get needParticipationParticipation => Intl.message("participation");
  String get needParticipationPurpose => Intl.message("purpose");
  String get needParticipationMattering => Intl.message("to matter");
  String get needParticipationUnderstanding => Intl.message("understanding");
}

class L10nDelegate extends LocalizationsDelegate<L10n> {
  const L10nDelegate();

  @override
  bool isSupported(Locale locale) => L10n.SUPPORTED_LOCALES.any((Locale loc) {
        return loc.languageCode == locale.languageCode;
      }); //todo may be inefficient

  @override
  Future<L10n> load(Locale locale) => L10n.load(locale);

  @override
  bool shouldReload(L10nDelegate old) => false;
}
