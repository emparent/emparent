// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emparent/l10n/l10n.dart';
import 'package:emparent/models/idea.dart';
import 'package:emparent/models/need.dart';
import 'package:emparent/models/situation_person.dart';
import 'package:emparent/widgets/avatar.dart';
import 'package:emparent/widgets/inline_app_bar.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:emparent/models/person.dart';
import 'package:emparent/widgets/people_dialog.dart';
import 'package:emparent/database/dbhelper.dart';
import 'package:emparent/models/topic.dart';
import 'package:emparent/models/situation.dart';
import 'package:emparent/widgets/situation_calendar.dart';
import 'package:emparent/screens/page_mixin.dart';

class Situation4Solutions extends StatefulWidget {
  final int situationId;
  final List<DBPersonInSituation> peopleInSituation;

  Situation4Solutions(
      {Key key, @required this.situationId, @required this.peopleInSituation})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return Situation4SolutionsState();
  }
}

class Situation4SolutionsState extends State<Situation4Solutions> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: DBHelper.db.queryIdeasBySituation(widget.situationId),
        builder: (BuildContext context, AsyncSnapshot<List<DBIdea>> snapshot) {
          if (snapshot.hasData) {
            List<Widget> ideaWidgets = _buildIdeas(snapshot.data);

            List<Widget> widgetList = [
              InlineAppBar(
                  title: Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 12, horizontal: 4),
                      child:
                          Text(L10n.of(context).textThinkUpMultipleSolutions)))
            ]
              ..addAll(_buildNeeds(widget.peopleInSituation))
              ..add(
                (Padding(
                    padding: EdgeInsets.only(top: 16, left: 16),
                    child: Text(L10n.of(context).textToMeetAllNeeds,
                        style: Theme.of(context).textTheme.caption))),
              )
              ..addAll(ideaWidgets)
              ..add(
                  FlatButton(
      child: Text(L10n.of(context).btnAddIdea),
      onPressed: () async {
        await DBHelper.db.insertIdea(widget.situationId, "");
        //todo set focus in last element of listview
        setState(() {});
      },
    )); //todo use ... expand operator with Dart 2.3

            return SingleChildScrollView(
                child: Column(
              children: widgetList,
              crossAxisAlignment: CrossAxisAlignment.start,
            ));
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }

  List<Widget> _buildNeeds(List<DBPersonInSituation> peopleInSituation) {
    return peopleInSituation.map((DBPersonInSituation personInSituation) {
      var person = personInSituation.person;
      return FutureBuilder(
        future:
            DBHelper.db.queryNeedsBySitPerson(widget.situationId, person.id),
        builder: (_, AsyncSnapshot<List<String>> snapshot) {
          String needHeading = (person.id == DBHelper.YOU_ID)
              ? L10n.of(context).genderYouNeeded(person.she ? "female" : "male")
              : L10n.of(context).genderTheyNeeded(
                  person.she ? "female" : "male", person.name);

          if (snapshot.hasData) {
            List<String> translatedNeeds = snapshot.data
                .map((String key) => Need.getTranslation(context, key))
                .toList();
            return Row(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(left: 16, right: 8, bottom: 8),
                    child: PersonAvatar(
                      radius: 24,
                      person: person,
                    )), //todo figure out how to get person.avatar inside
                Flexible(
                    child: Column(
                  children: [
                    Text(needHeading,
                        style: Theme.of(context).textTheme.caption),
                    Text(
                      L10n.of(context).getPluralList(translatedNeeds),
                      softWrap: true,
                    )
                  ],
                  crossAxisAlignment: CrossAxisAlignment.start,
                ))
              ],
            );
          } else
            return Center(child: CircularProgressIndicator());
        },
      );
    }).toList();
  }

  List<Widget> _buildIdeas(List<DBIdea> ideas) {
    return ideas.map(
      (DBIdea idea) {
        var controller = TextEditingController(text: idea.idea);
        controller.addListener(() async {
          await DBHelper.db.updateIdeaStr(idea.id, controller.text);
        });
        return ListTile(
          title: TextField(
            decoration: InputDecoration(
                border: InputBorder.none,
                filled: true,
                hintText: L10n.of(context).textSolutionIdea),
            controller: controller,
          ),
          trailing: IconButton(
            icon: Icon(Icons.delete),
            onPressed: () {
              DBHelper.db.deleteIdea(idea.id);
              setState(() {});
            },
          ),
        );
      },
    ).toList();
    // todo info about needs
  }
}
