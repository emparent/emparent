// Copyright (C) 2019 Miroslav Mazel
//
// This file is part of Emparent.
//
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emotionpicker/emotion.dart';
import 'package:emparent/models/need.dart';
import 'package:emparent/models/situation_person.dart';
import 'package:emparent/widgets/inline_app_bar.dart';
import 'package:emparent/widgets/need_dialog.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:emparent/models/person.dart';
import 'package:emparent/widgets/people_dialog.dart';
import 'package:emparent/database/dbhelper.dart';
import 'package:emparent/models/topic.dart';
import 'package:emparent/models/situation.dart';
import 'package:emparent/screens/page_mixin.dart';
import 'package:emparent/l10n/l10n.dart';

class Situation3Needs extends StatefulWidget {
  final int situationId;
  final DBPersonInSituation personInSituation;

  Situation3Needs(
      {Key key, @required this.situationId, @required this.personInSituation})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _Situation3NeedsState();
  }
}

class _Situation3NeedsState extends State<Situation3Needs> {
  static const _BOTTOM_BAR_HEIGHT = 52.0;

  @override
  Widget build(BuildContext context) {
    var person = widget.personInSituation.person;
    var emotionList = Emotion.getEmotions(context);
    return FutureBuilder<List<String>>(
        future:
            DBHelper.db.queryNeedsBySitPerson(widget.situationId, person.id),
        builder: (BuildContext context, AsyncSnapshot<List<String>> snapshot) {
          if (snapshot.hasData) {
            return Column(children: [
              InlineAppBar(
                  title: FutureBuilder(
                      future: DBHelper.db.queryEmotionsBySitPerson(
                          widget.situationId, person.id),
                      builder: (_, AsyncSnapshot snapshot) {
                        if (snapshot.hasData) {
                          List<Emotion> emotions = snapshot.data;

                          String titleText;
                          if (emotions.isEmpty)
                            titleText =
                                L10n.of(context).textHaveToChooseEmotions;
                          else {
                            List<String> emotionStrs =
                                emotions.map((Emotion e) {
                              return (emotionList[e.emotion])[e.intensity];
                            }).toList();
                            titleText = (person.id == DBHelper.YOU_ID)
                                ? L10n.of(context)
                                    .genderYouFeltBecauseYouNeeded(
                                        person.she ? "female" : "male",
                                        L10n.of(context)
                                            .getPluralList(emotionStrs))
                                : L10n.of(context)
                                    .genderTheyFeltBecauseTheyNeeded(
                                        person.she ? "female" : "male",
                                        L10n.of(context)
                                            .getPluralList(emotionStrs),
                                        person.name);
                          }

                          return Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 4),
                              child: Text(
                                titleText,
                              ));
                        } else
                          return Center(child: CircularProgressIndicator());
                      })),
              Expanded(child: _buildContent(snapshot.data, person))
            ]);
          } else
            return Center(child: CircularProgressIndicator());
        });
  }

  Widget _buildContent(List<String> selectedKeys, DBPerson person) {
    Widget switchSliver = SliverList(
      delegate: SliverChildListDelegate([
        person.id == DBHelper.YOU_ID
            ? Container() //todo use an if statement within containing list
            : SwitchListTile(
                title: Text(L10n.of(context)
                    .genderTheirOwnDescription(person.she ? "female" : "male")),
                value: widget.personInSituation.ownNeedDesc,
                onChanged: (bool newValue) {
                  setState(() {
                    DBHelper.db.updateSitPerOwnNeed(
                        widget.situationId, person.id, newValue);
                    widget.personInSituation.ownNeedDesc = newValue;
                  });
                },
              )
      ]),
    );

    return Stack(children: [
      Container(
          padding: EdgeInsets.fromLTRB(
              8, 8, 8, selectedKeys.length > 0 ? _BOTTOM_BAR_HEIGHT : 0),
          child: CustomScrollView(slivers: [
            SliverGrid.count(
                crossAxisCount: 2,
                children: _buildCards(selectedKeys),
                childAspectRatio: 1.5),
            switchSliver
          ])),
      Align(
          alignment: FractionalOffset.bottomLeft,
          child: Container(
              constraints: BoxConstraints.expand(height: _BOTTOM_BAR_HEIGHT),
              child: _buildBottomBar(selectedKeys)))
    ]);
  }

  Widget _buildAddNeedDialog(BuildContext context) {
    TextEditingController controller = TextEditingController();
    return AlertDialog(
        title: Text(L10n.of(context).textAddBasicNeed),
        content: SingleChildScrollView(
            child: Column(
          children: [
            TextField(
              controller: controller,
              autofocus: true,
              decoration: InputDecoration(
                border: InputBorder.none,
                filled: true,
                hintText: L10n.of(context).textBasicNeed,
              ),
            ),
            Padding(
                padding: EdgeInsets.only(top: 8),
                child: Text(
                  L10n.of(context).textFocusOnBasicHumanNeeds,
                  style: Theme.of(context).textTheme.caption,
                ))
          ],
          mainAxisSize: MainAxisSize.min,
        )),
        actions: <Widget>[
          FlatButton(
            child: Text(L10n.of(context).btnCancel),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
            child: Text(L10n.of(context).btnAdd),
            onPressed: () async {
              await DBHelper.db.insertPersonNeed(widget.situationId,
                  widget.personInSituation.person.id, controller.text);
              Navigator.of(context).pop();
            },
          ),
        ]);
  }

  List<Widget> _buildCards(List<String> selectedKeys) {
    var needGroupNames = Need.needTranslationKeys.keys;
    return List.generate(needGroupNames.length + 1, (int i) {
      if (i == needGroupNames.length) {
        return FlatButton(
          child: Text(L10n.of(context).btnAddNeed),
          onPressed: () async {
            showDialog(context: context, builder: _buildAddNeedDialog)
                .then((_) {
              setState(() {});
            });
          },
        );
      } else {
        String needGroupNameKey = needGroupNames.elementAt(i);
        List<String> needGroupKeys = Need.needTranslationKeys[needGroupNameKey];
        String needGroupNameTranslation =
            Need.getTranslation(context, needGroupNameKey);
        return Card(
          child: InkWell(
              child: ListTile(
            title: Padding(
              child: Text(needGroupNameTranslation),
              padding: EdgeInsets.only(top: 12, bottom: 4),
            ),
            subtitle: Text(
              needGroupKeys
                  .map((String key) => Need.getTranslation(context, key))
                  .join(", "),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ), //todo list as many needs as possible
            onTap: () async {
              _showNeedDialog(
                  needGroupNameTranslation, needGroupKeys, selectedKeys);
            },
          )),
        );
      }
    });
  }

  Future<void> _showNeedDialog(String needGroupName, List<String> groupNeedKeys,
      List<String> selectedKeys) {
    showDialog(
        context: context,
        builder: (_) => NeedDialog(
              groupNameTranslation: needGroupName,
              groupNeedKeys: groupNeedKeys,
              selectedKeys: selectedKeys,
              situationId: widget.situationId,
              personId: widget.personInSituation.person.id,
            )).then((_) {
      setState(() {});
    });
  }

  Widget _buildBottomBar(List<String> selectedKeys) {
    return (selectedKeys.length > 0)
        ? BottomAppBar(
            child: ListView.builder(
              padding: EdgeInsets.all(8),
              itemCount: selectedKeys.length,
              itemBuilder: (context, index) {
                String needKey = selectedKeys[index];

                return Padding(
                  child: Chip(
                    label: Text(Need.getTranslation(context, needKey)),
                    onDeleted: () {
                      setState(() {
                        selectedKeys.remove(
                            needKey); //todo might be faster to use index
                        DBHelper.db.deletePersonNeed(widget.situationId,
                            widget.personInSituation.person.id, needKey);
                      });
                    },
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                );
              },
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
            ),
          )
        : Container(); //todo use an if statement within containing list
  }
}
