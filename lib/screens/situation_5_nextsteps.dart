// Copyright (C) 2019 Miroslav Mazel
//
// This file is part of Emparent.
//
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emparent/database/asset_paths.dart';
import 'package:emparent/l10n/l10n.dart';
import 'package:emparent/widgets/inline_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Situation5NextSteps extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      InlineAppBar(
          title: Padding(
              padding: EdgeInsets.symmetric(vertical: 12, horizontal: 4),
              child: Text(L10n.of(context).textUseEmpathyToFindSolution))),
      Expanded(
          child: SingleChildScrollView(
              padding: EdgeInsets.all(16),
              child: Column(children: [
                Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Text(L10n.of(context).textApplyInPractice)),
                StepRow(
                    "1-empathy.png",
                    _getFormattedText(
                        L10n.of(context).textGuide1DiscoverEmotionsNeeds)),
                StepRow(
                    "2-assurance.png",
                    _getFormattedText(
                        L10n.of(context).textGuide2CheckEmotionsNeeds)),
                StepRow(
                    "3-communication.png",
                    _getFormattedText(
                        L10n.of(context).textGuide3ShareEmotionsNeeds)),
                StepRow(
                    "4-understanding.png",
                    _getFormattedText(
                        L10n.of(context).textGuide4ConfirmUnderstanding)),
                StepRow(
                    "5-ideas.png",
                    _getFormattedText(
                        L10n.of(context).textGuide5WorkTowardSolution)),
                FlatButton(
                    child: Text(
                      L10n.of(context).btnShowVideoExample,
                    ),
                    onPressed: () async {
                      String url = L10n.of(context).urlVideoExample;
                      if (await canLaunch(url)) {
                        await launch(url);
                      } else {
                        throw 'Could not launch $url';
                      }
                    }),
                Padding(
                    padding: EdgeInsets.only(top: 8),
                    child: Text(
                      L10n.of(context).textNotSureInThisCase,
                      style: Theme.of(context).textTheme.caption,
                    )),
                RaisedButton(
                    child: Text(
                      L10n.of(context).btnTalkWithOthers,
                    ),
                    onPressed: () async {
                      String url = L10n.of(context).urlCommunity;
                      if (await canLaunch(url)) {
                        await launch(url);
                      } else {
                        throw 'Could not launch $url';
                      }
                    })
              ])))
    ]);
  }

  List<TextSpan> _getFormattedText(String stringWithBold) {
    var substrings = stringWithBold.split("**");
    return List.generate(substrings.length, (int i) {
      return TextSpan(
          text: substrings[i],
          style: (i % 2 == 0) ? null : TextStyle(fontWeight: FontWeight.bold));
    });
  }
}

class StepRow extends StatelessWidget {
  final String assetName;
  final List<TextSpan> textSpans;

  StepRow(this.assetName, this.textSpans);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Padding(
            padding: EdgeInsets.only(right: 8),
            child: Image.asset("${Assets.ASSET_FOLDER}$assetName")),
        Flexible(
            child: RichText(
                text: TextSpan(
                    style: Theme.of(context).textTheme.body1,
                    children: textSpans),
                softWrap: true))
      ],
    );
  }
}
