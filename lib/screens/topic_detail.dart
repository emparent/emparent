// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emotionpicker/emotion.dart';
import 'package:emparent/l10n/l10n.dart';
import 'package:emparent/models/idea.dart';
import 'package:emparent/models/need.dart';
import 'package:emparent/models/person.dart';
import 'package:emparent/models/situation_person.dart';
import 'package:emparent/widgets/avatar.dart';
import 'package:emparent/widgets/avatar_row.dart';
import 'package:flutter/material.dart';
import 'package:emparent/models/topic.dart';
import 'package:emparent/models/situation.dart';
import 'package:emparent/screens/situation_detail.dart';
import 'package:emparent/database/dbhelper.dart';
import 'package:intl/intl.dart';

// todo

class TopicDetail extends StatefulWidget {
  final DBTopic
      topic; //todo change to topicId, so topic can be renamed from this screen

  TopicDetail({Key key, @required this.topic}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _TopicDetailState();
  }
}

class _TopicDetailState extends State<TopicDetail> {
  static const String _MENU_DELETE = 'Delete';
  static const String _MENU_RESOLVE = 'Resolve';
  static const String _MENU_EDIT = 'Edit';

  @override
  Widget build(BuildContext context) {
    Widget _list = _buildList();

    return Scaffold(
        appBar: _buildNormalAppBar(),
        body: _list,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton.extended(
          label: Text(L10n.of(context).btnFollowUpSituation),
          icon: Icon(Icons.add),
          onPressed: () {
            _followupSituation();
          },
        ));
  }

  Widget _buildNormalAppBar() {
    Text title;
    if (widget.topic.title == null || widget.topic.title == "")
      title = Text(
        L10n.of(context).textUntitled,
        style: TextStyle(fontStyle: FontStyle.italic),
      );
    else
      title = Text(widget.topic.title);

    return AppBar(
      title: title,
      actions: <Widget>[
        PopupMenuButton(
            itemBuilder: (_) => <PopupMenuItem<String>>[
                  PopupMenuItem<String>(
                      child: Text(widget.topic.resolved
                          ? L10n.of(context).btnUnresolve
                          : L10n.of(context).btnResolve), //todo test if updates
                      value: _MENU_RESOLVE),
                  PopupMenuItem<String>(
                      child: Text(L10n.of(context).btnDeleteTopic),
                      value: _MENU_DELETE)
                ],
            onSelected: (String value) {
              if (value == _MENU_DELETE) deleteAndLeave();
              if (value == _MENU_RESOLVE)
                widget.topic.resolved
                    ? unresolve()
                    : resolve(); // todo allow unresolving
            })
      ],
    );
  }

  void deleteAndLeave() {
    DBHelper.db.deleteTopic(widget.topic.id);
    Navigator.pop(context, true);
  }

  void resolve() {
    DBHelper.db.updateTopicResolution(widget.topic.id, true);
    widget.topic.resolved = true;
  }

  void unresolve() {
    DBHelper.db.updateTopicResolution(widget.topic.id, false);
    widget.topic.resolved = false;
  }

  Widget _buildList() {
    return FutureBuilder<List<DBSituation>>(
        future: DBHelper.db.queryTopicSituations(widget.topic.id),
        builder:
            (BuildContext context, AsyncSnapshot<List<DBSituation>> snapshot) {
          if (snapshot.hasData) {
            List<DBSituation> situations = snapshot.data;
            return ListView.separated(
                separatorBuilder: (context, index) => Divider(),
                itemCount: situations.length,
                itemBuilder: (BuildContext context, int position) {
                  return _buildListItem(
                      situations[position], position == situations.length - 1);
                },
                padding: EdgeInsets.only(bottom: 72));
          } else {
            return Center(
              child: LinearProgressIndicator(),
            );
            //todo return empty
          }
        });
  }

  Widget _buildListItem(DBSituation situation, bool initiallyExpanded) {
    return FutureBuilder(
        future: DBHelper.db.queryPeopleInSituation(situation.id),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            List<DBPersonInSituation> peopleInSituation = snapshot.data;
            return ExpansionTile(
              title: Text(DateFormat.yMMMd().format(
                  DateTime.fromMillisecondsSinceEpoch(
                      situation.dateMilliseconds))),
              children: _buildExpandedContent(situation, peopleInSituation),
              initiallyExpanded: initiallyExpanded,
              trailing: _buildListTrailing(situation, peopleInSituation,
                  true), //todo get expanded bool from widget
            );
          } else
            return Center(
              child: CircularProgressIndicator(),
            );
        });
  }

  Widget _buildListTrailing(DBSituation situation,
      List<DBPersonInSituation> peopleInSit, bool expanded) {
    Widget avatarRow = AvatarRow(
      people: peopleInSit.map((personInSit) => personInSit.person).toList(),
      radius: 20,
    );
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        avatarRow,
        PopupMenuButton(
            itemBuilder: (_) => <PopupMenuItem<String>>[
                  PopupMenuItem<String>(
                      child: Text(L10n.of(context).btnEdit), value: _MENU_EDIT),
                  PopupMenuItem<String>(
                      child: Text(L10n.of(context).btnDelete),
                      value: _MENU_DELETE),
                ],
            onSelected: (String value) {
              switch (value) {
                case _MENU_EDIT:
                  _showDetail(situation);
                  setState(() {}); //todo not working for title
                  break;
                case _MENU_DELETE:
                  DBHelper.db.deleteSituation(situation.id);
                  setState(() {});
                  break;
              }
              ;
            })
      ],
    );
  }

  List<Widget> _buildExpandedContent(
      DBSituation situation, List<DBPersonInSituation> people) {
    return [_buildDescription(situation)] //todo spread operator
      ..addAll(_buildEmotionsAndNeeds(situation, people))
      ..add(_buildIdeas(situation.id));
  }

  Widget _buildDescription(DBSituation situation) {
    if (situation.description != null && situation.description.length > 0) {
      return Container(
        child: Text(situation.description),
        alignment: Alignment.centerLeft,
        margin: EdgeInsets.fromLTRB(16, 0, 16, 16),
      );
    } else
      return Container(); //todo use an if statement within containing list
  }

  List<Widget> _buildEmotionsAndNeeds(
      DBSituation situation, List<DBPersonInSituation> peopleInSit) {
    var emotionList = Emotion.getEmotions(context);
    return peopleInSit.map<Widget>((DBPersonInSituation personInSit) {
      var person = personInSit.person;
      return FutureBuilder(
        future: DBHelper.db.queryEmotionsBySitPerson(situation.id, person.id),
        builder: (_, AsyncSnapshot<List<Emotion>> snapshot) {
          if (snapshot.hasData) {
            List<Emotion> emotions = snapshot.data;
            return FutureBuilder(
                future:
                    DBHelper.db.queryNeedsBySitPerson(situation.id, person.id),
                builder: (_, AsyncSnapshot<List<String>> snapshot) {
                  String emoHeading =
                      _getEmotionHeading(person); //todo wrap lines
                  String needHeading = _getNeedHeading(person);

                  if (snapshot.hasData) {
                    List<String> translatedNeeds = snapshot.data
                        .map((String needKey) =>
                            Need.getTranslation(context, needKey))
                        .toList();
                    List<String> translatedEmotions = emotions.map((Emotion e) {
                      return (emotionList[e.emotion])[e.intensity];
                    }).toList();

                    return Padding(
                        padding: EdgeInsets.only(bottom: 12),
                        child: Row(
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.only(left: 16, right: 8),
                                child: PersonAvatar(
                                  radius: 24,
                                  person: person,
                                )), //todo figure out how to get person.avatar inside
                            Flexible(
                                child: Column(
                              children: [
                                Text(emoHeading,
                                    style: Theme.of(context).textTheme.caption),
                                Text(
                                  L10n.of(context)
                                      .getPluralList(translatedEmotions),
                                  softWrap: true,
                                ),
                                Text(
                                  needHeading,
                                  style: Theme.of(context).textTheme.caption,
                                  softWrap: true,
                                ),
                                Text(L10n.of(context)
                                    .getPluralList(translatedNeeds))
                              ],
                              crossAxisAlignment: CrossAxisAlignment.start,
                            ))
                          ],
                        ));
                  } else
                    return Center(child: CircularProgressIndicator());
                });
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      );
    }).toList();
  }

  Widget _buildIdeas(int situationId) {
    return Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.only(left: 16),
        child: FutureBuilder(
            future: DBHelper.db.queryIdeasBySituation(situationId),
            builder: (_, AsyncSnapshot<List<DBIdea>> snapshot) {
              if (snapshot.hasData) {
                List<DBIdea> ideas = snapshot.data;
                return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: List.generate(
                        ideas.length > 0 ? ideas.length + 1 : 0, (int i) {
                      if (i == 0)
                        return Container(
                            child: Text(L10n.of(context).textToMeetAllNeeds,
                                style: Theme.of(context).textTheme.caption),
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(bottom: 12));
                      else
                        return Padding(
                            padding: EdgeInsets.only(bottom: 8),
                            child: Row(children: [
                              CircleAvatar(
                                child: Text("${i}"),
                                radius: 12,
                                backgroundColor: Colors.white10,
                                foregroundColor: Colors.white,
                              ),
                              Padding(
                                  padding: EdgeInsets.only(left: 8),
                                  child: Text(ideas[i - 1].idea))
                            ]));
                    }));
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            }));
  }

  String _getEmotionHeading(DBPerson person) {
    if (person.id == DBHelper.YOU_ID) {
      return L10n.of(context).genderYouFelt(person.she ? "female" : "male");
    } else {
      return L10n.of(context)
          .genderTheyFelt(person.she ? "female" : "male", person.name);
    }
  }

  String _getNeedHeading(DBPerson person) {
    if (person.id == DBHelper.YOU_ID) {
      return L10n.of(context)
          .genderBecauseYouNeeded(person.she ? "female" : "male");
    } else {
      return L10n.of(context)
          .genderBecauseTheyNeeded(person.she ? "female" : "male");
    }
  }

  void _showDetail(DBSituation situation) async {
    await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return SituationDetail(situation: situation, topic: widget.topic);
    }));
  }

  void _followupSituation() async {
    DBSituation situation = await DBHelper.db.createSituation(widget.topic.id);

    await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return SituationDetail(topic: widget.topic, situation: situation);
    }));
  }
}
