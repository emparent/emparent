// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emparent/database/sphelper.dart';
import 'package:emparent/l10n/l10n.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class MainNavigator extends StatelessWidget {
  static const String HOME = '/home';
  static const String ONBOARDING = '/onboarding';

  static const _NOTIFICATION_HOUR = 20; //todo check that this works
  static const _NOTIFICATION_CHANNEL_ID = "daily reminder";

  @override
  Widget build(BuildContext context) {
    _setDailyNotification(context);

    SPHelper.showingOnboarding().then((showOnboarding) {
      if (showOnboarding) {
        Navigator.pushReplacementNamed(context, ONBOARDING);
      } else {
        Navigator.pushReplacementNamed(context, HOME);
      }
    });

    return Center(
      child: CircularProgressIndicator(),
    );
  }

  void _setDailyNotification(BuildContext context) async {
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
// initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    var initializationSettingsAndroid =
        AndroidInitializationSettings('ic_notification');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: (int id,
            String title,
            String body,
            String
                payload) async {}); //todo might be unnecessarily efficient, try replacing with null
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (String payload) async {
      return await Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => MainNavigator()),
      );
    });

    var time = Time(_NOTIFICATION_HOUR, 0, 0);
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        _NOTIFICATION_CHANNEL_ID,
        L10n.of(context).textDailyReminder,
        L10n.of(context).textReminderToRecordSituation,
        color: Color(0xff99B433)); //todo not sure if color is used right
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.showDailyAtTime(
        0,
        L10n.of(context).textTimeToRecordSituation,
        L10n.of(context).textWellGuideYouToSolution,
        time,
        platformChannelSpecifics);
  }
}
