// Copyright (C) 2019 Miroslav Mazel
//
// This file is part of Emparent.
//
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:collection';

import 'package:emotionpicker/emotion.dart';
import 'package:emotionpicker/emotion_canvas.dart';
import 'package:emotionpicker/emotion_display.dart';
import 'package:emparent/l10n/l10n.dart';
import 'package:emparent/models/person.dart';
import 'package:emparent/models/situation_person.dart';
import 'package:emparent/widgets/inline_app_bar.dart';
import 'package:emparent/widgets/selection_bar.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:emparent/widgets/people_dialog.dart';
import 'package:emparent/database/dbhelper.dart';
import 'package:emparent/models/topic.dart';
import 'package:emparent/models/situation.dart';
import 'package:emparent/screens/page_mixin.dart';

class Situation2Emotions extends StatefulWidget {
  final int situationId;
  final DBPersonInSituation personInSituation;

  Situation2Emotions(
      {Key key, @required this.situationId, @required this.personInSituation})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _Situation2EmotionsState();
  }
}

class _Situation2EmotionsState extends State<Situation2Emotions> {
  static const _BOTTOM_BAR_HEIGHT = 52.0;
  static const _BOTTOM_SWITCH_HEIGHT = 52.0;
  Emotion _emotion;

  @override
  Widget build(BuildContext context) {
    var person = widget.personInSituation.person;
    String titleText = (person.id == DBHelper.YOU_ID)
        ? L10n.of(context).genderHowDidYouFeel(person.she ? "female" : "male")
        : L10n.of(context)
            .genderHowDidTheyFeel(person.she ? "female" : "male", person.name);

    var emotionList = Emotion.getEmotions(context);

    return FutureBuilder<List<Emotion>>(
        future: DBHelper.db.queryEmotionsBySitPerson(
            widget.situationId, widget.personInSituation.person.id),
        builder: (BuildContext context, AsyncSnapshot<List<Emotion>> snapshot) {
          if (snapshot.hasData) {
            var person = widget.personInSituation.person;
            var emoSwitch = person.id == DBHelper.YOU_ID
                ? Container() //todo use an if statement within containing list
                : Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: _BOTTOM_BAR_HEIGHT),
                      child: FormField(
                          builder: (FormFieldState<SwitchListTile> state) {
                        return SwitchListTile(
                          title: Text(L10n.of(context)
                              .genderTheirOwnDescription(
                                  person.she ? "female" : "male")),
                          value: widget.personInSituation.ownEmotionDesc,
                          onChanged: (bool newValue) {
                            setState(() {
                              DBHelper.db.updateSitPerOwnEmotion(
                                  widget.situationId, person.id, newValue);
                              widget.personInSituation.ownEmotionDesc =
                                  newValue;
                            });
                          },
                        );
                      }),
                    ));
            var chosenEmotions = snapshot.data;

            return Column(children: [
              InlineAppBar(
                  title: Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 12, horizontal: 4),
                      child: Text(titleText))),
              Expanded(
                  child: Stack(children: [
                Align(
                  child: _emotion == null
                      ? Container()
                      : _emotion.intensity == null
                          ? Text(
                              L10n.of(context).btnCancel.toUpperCase(),
                              style: Theme.of(context).textTheme.display1,
                            )
                          : Text(
                              (emotionList[_emotion.emotion])[
                                  _emotion.intensity],
                              style: Theme.of(context)
                                  .textTheme
                                  .display1
                                  .copyWith(color: Colors.white30),
                            ),
                  alignment: FractionalOffset.topCenter,
                ),
                Padding(
                    padding: EdgeInsets.fromLTRB(
                        8, 8, 8, _BOTTOM_BAR_HEIGHT + _BOTTOM_SWITCH_HEIGHT),
                    child: (Align(
                        alignment: FractionalOffset.center,
                        child: EmotionCanvas(onChanged: (Emotion emotion) {
                          setState(() {
                            _emotion = emotion;
                          });
                        }, onValueSet: (Emotion emotion) {
                          setState(() {
                            if (emotion != null) {
                              chosenEmotions.add(emotion);
                              DBHelper.db.insertPersonEmotion(
                                  widget.situationId,
                                  person.id,
                                  emotion.emotion,
                                  emotion.intensity);
                            }
                            _emotion = null;
                          });
                        })))),
                emoSwitch,
                if (_emotion == null)
                  Align(
                      alignment: FractionalOffset.bottomLeft,
                      child: Container(
                          constraints:
                              BoxConstraints.expand(height: _BOTTOM_BAR_HEIGHT),
                          child: SelectionBar(
                            itemMap: LinkedHashMap.fromIterables(
                                List.generate(chosenEmotions.length, (index) {
                                  var emotion = chosenEmotions[index];
                                  return (emotionList[emotion.emotion])[
                                      emotion.intensity];
                                }),
                                chosenEmotions),
                            getColor: (dynamic em) {
                              return Color(
                                  EmotionDisplay.basicColorsLight[em.emotion]);
                            },
                            darkText: true,
                            onItemRemoved: (dynamic em) {
                              DBHelper.db.deletePersonEmotion(
                                  widget.situationId,
                                  widget.personInSituation.person.id,
                                  em.emotion,
                                  em.intensity);
                            },
                          ))),
                if (_emotion != null)
                  Container(
                      alignment: FractionalOffset.bottomCenter,
                      padding:
                          EdgeInsets.symmetric(vertical: 36, horizontal: 0),
                      child: ClipOval(
                          child: Material(
                              color: _emotion.intensity == null
                                  ? Theme.of(context).colorScheme.background
                                  : Theme.of(context)
                                      .colorScheme
                                      .onBackground
                                      .withAlpha(127),
                              child: SizedBox(
                                  height: 24,
                                  width: 24,
                                  child: Icon(
                                    Icons.cancel,
                                    color: _emotion.intensity == null
                                        ? Theme.of(context)
                                            .colorScheme
                                            .onBackground
                                        : Theme.of(context)
                                            .colorScheme
                                            .background,
                                  ))))),
                if (_emotion != null)
                  Container(
                      alignment: FractionalOffset.bottomCenter,
                      padding:
                          EdgeInsets.symmetric(vertical: 16, horizontal: 0),
                      child: Opacity(
                          opacity: _emotion.intensity == null ? 1 : 0.5,
                          child: Text("Drag here to cancel")))
              ]))
            ]);
          } else
            return Center(child: CircularProgressIndicator());
        });
  }
}
