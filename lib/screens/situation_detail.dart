// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emparent/database/sphelper.dart';
import 'package:emparent/l10n/l10n.dart';
import 'package:emparent/models/person.dart';
import 'package:emparent/models/situation_person.dart';
import 'package:emparent/screens/onboarding.dart';
import 'package:emparent/screens/situation_5_nextsteps.dart';
import 'package:flutter/material.dart';
import 'package:emparent/screens/situation_1_description.dart';
import 'package:emparent/screens/situation_2_emotions.dart';
import 'package:emparent/screens/situation_3_needs.dart';
import 'package:emparent/screens/situation_4_solutions.dart';
import 'package:emparent/models/topic.dart';
import 'package:emparent/models/situation.dart';
import 'package:emparent/screens/page_mixin.dart';
import 'package:emparent/database/dbhelper.dart';
import 'package:flutter/services.dart';

class SituationDetail extends StatefulWidget {
  final DBTopic topic;
  final DBSituation situation;

  SituationDetail({Key key, @required this.topic, @required this.situation})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SituationDetailState();
  }
}

class SituationDetailState extends State<SituationDetail> {
  //todo recycle pages somehow?
  static const int _INIT_PAGE = 0;
  List<Widget> _defaultPagesStart;
  List<Widget> _pages;

  final GlobalKey<Situation1DescriptionState> _firstPageKey = GlobalKey<
      Situation1DescriptionState>(); //todo really ugly architecture - rethink!!!
  PageController _pageController = PageController(initialPage: _INIT_PAGE);
  // Widget _appBarTitle;
  double _progress = 0.0;
  Widget _situation5next = Situation5NextSteps();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          sized: false,
          value: SystemUiOverlayStyle.dark
              .copyWith(statusBarIconBrightness: Brightness.light),
          child: SafeArea(child: _buildContent()),
        ),
        bottomNavigationBar: _getNavigationBar());
  }

  Widget _buildContent() {
    return FutureBuilder(
        future: DBHelper.db.queryAllPeople(),
        builder:
            (BuildContext context, AsyncSnapshot<List<DBPerson>> snapshot) {
          if (snapshot.hasData) {
            List<DBPerson> allPeople = snapshot.data;
            DBPerson youPerson = allPeople[0];
            for (var person in allPeople) {
              if (person.id == DBHelper.YOU_ID) {
                youPerson = person;
                break;
              }
            }

            var fakeYouPersonInSit = DBPersonInSituation(
                person:
                    youPerson); // can fake this, as YOU doesn't have other fields per situation

            _defaultPagesStart = <Widget>[
              Situation1Description(
                allPeople: allPeople,
                topic: widget.topic,
                situation: widget.situation,
                key: _firstPageKey,
              ),
              Situation2Emotions(
                situationId: widget.situation.id,
                personInSituation: fakeYouPersonInSit,
              ),
              Situation3Needs(
                situationId: widget.situation.id,
                personInSituation: fakeYouPersonInSit,
              )
            ];
            if (_pages == null) _pages = List.from(_defaultPagesStart);
            // _appBarTitle = _defaultPagesStart[_INIT_PAGE].getAppBarTitle();

            return PageView(
                children: _pages,
                onPageChanged: _onPageChanged,
                controller: _pageController,
                physics: NeverScrollableScrollPhysics());
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }

  Widget _getNavigationBar() {
    Widget backButton = (_progress > 0)
        ? FlatButton(
            child: Text(L10n.of(context).btnBack),
            onPressed: () {
              _pageController.previousPage(
                  duration: Duration(milliseconds: 300),
                  curve: Cubic(0.0, 0.0, 0.2, 1));
            },
          )
        : FlatButton(child: Text(L10n.of(context).btnBack), onPressed: null);

    Widget forwardButton = (_progress < 1)
        ? FlatButton(
            child: Text(L10n.of(context).btnNext),
            onPressed: () async {
              bool proceed = true;
              if (_progress == 0) {
                List<DBPersonInSituation> peopleInSituation = await DBHelper.db
                    .queryPeopleInSituation(widget.situation
                        .id); //todo do this on build, pass result to Description1
                        proceed = _firstPageKey.currentState.validateForm(peopleInSituation.length);
                await _refreshPages(peopleInSituation);
              }
              if (proceed) {
                _pageController.nextPage(
                    duration: Duration(milliseconds: 300),
                    curve: Cubic(0.0, 0.0, 0.2, 1));
              }
            },
          )
        : FlatButton(
            child: Text(L10n.of(context).btnDone),
            onPressed: () {
              Navigator.pop(context);
            },
          );

    return BottomAppBar(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          backButton,
          Expanded(child: LinearProgressIndicator(value: _progress)),
          forwardButton,
        ],
      ),
    );
  }

  Future<void> _refreshPages(
      List<DBPersonInSituation> peopleInSituation) async {
    List<Widget> tempPages = List.from(_defaultPagesStart);
    for (DBPersonInSituation personInSit in peopleInSituation) {
      if (personInSit.person.id != DBHelper.YOU_ID) {
        tempPages.add(Situation2Emotions(
          situationId: widget.situation.id,
          personInSituation: personInSit,
        ));
        tempPages.add(Situation3Needs(
          situationId: widget.situation.id,
          personInSituation: personInSit,
        ));
      }
    }

    tempPages.add(Situation4Solutions(
      situationId: widget.situation.id,
      peopleInSituation: peopleInSituation,
    ));

    tempPages.add(_situation5next);

    setState(() {
      _pages = tempPages;
    });

    return;
  }

  void _onPageChanged(int page) {
    setState(() {
      // _appBarTitle = _pages[page].getAppBarTitle();
      _progress = page / (_pages.length - 1);
    });
  }
}
