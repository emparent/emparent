// Copyright (C) 2019 Miroslav Mazel
//
// This file is part of Emparent.
//
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emparent/l10n/l10n.dart';
import 'package:emparent/widgets/checkbox_chip.dart';
import 'package:emparent/widgets/inline_app_bar.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:emparent/models/person.dart';
import 'package:emparent/widgets/people_dialog.dart';
import 'package:emparent/database/dbhelper.dart';
import 'package:emparent/models/topic.dart';
import 'package:emparent/models/situation.dart';
import 'package:emparent/screens/page_mixin.dart';
import 'package:emparent/widgets/situation_calendar.dart';

class Situation1Description extends StatefulWidget {
  //todo save on continue rather than on edit
  //todo refactor getAppBarTitle, should be in state
  final DBTopic topic;
  final DBSituation situation;
  final List<DBPerson> allPeople;

  Situation1Description(
      {Key key,
      @required this.topic,
      @required this.situation,
      @required this.allPeople})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => Situation1DescriptionState();
}

class Situation1DescriptionState extends State<Situation1Description> {
  bool validPeople = true;

  bool validateForm(int peopleInSituationCount) {
    setState(() {
      validPeople = peopleInSituationCount > 1;
    });
    return validPeople;
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      InlineAppBar(
          title: Padding(
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 4),
              child: Text.rich(
                TextSpan(
                  children: <InlineSpan>[
                    TextSpan(
                        text: L10n.of(this.context).textDescribeSituationFrom),
                    WidgetSpan(
                        alignment: PlaceholderAlignment.middle,
                        child:
                            SituationCalendarMenu(situation: widget.situation))
                  ],
                ),
              ))),
      Expanded(child: _buildContent())
    ]);
  }

  TextEditingController _titleController;
  TextEditingController _descController;

  @override
  void initState() {
    _titleController = TextEditingController(text: widget.topic.title);
    _descController = TextEditingController(text: widget.situation.description);

    _titleController.addListener(() async {
      await DBHelper.db
          .updateTopicTitle(widget.topic.id, _titleController.text);
    });

    _descController.addListener(() async {
      await DBHelper.db.updateSituationDescription(
          widget.situation.id, _descController.text);
    });

    DBHelper.db.insertSituationPerson(widget.situation.id, DBHelper.YOU_ID);

    super.initState();
  }

  Widget _buildContent() {
    //todo perhaps use Form class later
    return SingleChildScrollView(
        child: Container(
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
      child: Column(children: <Widget>[
        _buildTitle(),
        Padding(
            padding: EdgeInsets.only(top: 16),
            child: Text(
              L10n.of(context).textWhoWasInvolved,
              style: Theme.of(context).textTheme.caption,
            )),
        _buildPeopleRow(), //todo turn all of these (and others in other methods) into stateless widgets
        _buildPeopleWarning(), //todo use an if statement
        _buildManageButton(),
        _buildDescription(),
      ], crossAxisAlignment: CrossAxisAlignment.start),
    ));
  }

  Widget _buildTitle() {
    return TextField(
      decoration: InputDecoration(
          border: InputBorder.none,
          filled: true,
          labelText: L10n.of(context).textTitleOptional),
      controller: _titleController,
    );
  }

  Widget _buildDescription() {
    return Container(
      margin: EdgeInsets.only(top: 16.0),
      child: TextField(
          keyboardType: TextInputType.multiline,
          maxLines: null,
          decoration: InputDecoration(
              border: InputBorder.none,
              filled: true,
              labelText: L10n.of(context).textNotesOptional),
          controller: _descController,
          textInputAction: TextInputAction.done),
    );
    //todo add IconButton(icon: Icon(Icons.info))
  }

  Widget _buildPeopleRow() {
    if (widget.allPeople.length > 1) {
      return Wrap(
        children: _buildPeopleChips(widget.allPeople),
        spacing: 8.0,
      );
    } else {
      return Text(L10n.of(context).textHaveToAddInvoled);
    }
  }

  Widget _buildPeopleWarning() {
    //todo use an if statement within containing list
    return validPeople
        ? Container()
        : Text(
            L10n.of(context).textHaveToAddOneMoreInvolved,
            style:
                Theme.of(context).textTheme.caption.copyWith(color: Colors.red),
          );
  }

  Widget _buildManageButton() {
    var onPressedAction = () async {
      await showDialog(context: context, builder: (_) => PeopleDialog());
      DBHelper.db.queryAllPeople().then((List<DBPerson> newPeople) {
        setState(() {
          widget.allPeople.replaceRange(0, widget.allPeople.length, newPeople);
        });
      });
    };

    return (widget.allPeople.length > 1)
        ? FlatButton(
            child: Text(L10n.of(context).btnManagePeople),
            onPressed: onPressedAction)
        : RaisedButton(
            //todo if only two allPeople, both always enabled
            child: Text(L10n.of(context).btnAddPeople),
            onPressed: onPressedAction,
          );
  }

  List<Widget> _buildPeopleChips(List<DBPerson> allPeople) {
    List<Widget> peopleChoiceChips = List<Widget>();
    for (DBPerson person in allPeople) {
      Widget avatarImage;
      if (person.avatar != null)
        avatarImage = Image.file(File(person.avatar));
      else
        avatarImage = Icon(Icons.account_circle);

      peopleChoiceChips.add(FutureBuilder<bool>(
          future:
              DBHelper.db.querySituationPerson(widget.situation.id, person.id),
          builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
            if (person.id == DBHelper.YOU_ID)
              return ChoiceChip(
                label: Text(L10n.of(context).textYou),
                avatar: avatarImage,
                selected: true,
              );
            else
              return CheckboxChip(
                label: Text(person.name),
                avatar: avatarImage,
                selected: person.id == DBHelper.YOU_ID
                    ? true
                    : (snapshot.hasData) ? snapshot.data : false,
                onSelected: (bool selected) {
                  if (selected) {
                    DBHelper.db
                        .insertSituationPerson(widget.situation.id, person.id);
                    validPeople = true;
                  } else {
                    DBHelper.db
                        .deleteSituationPerson(widget.situation.id, person.id);
                  }
                  setState(() {});
                },
              );
          }));
    }
    return peopleChoiceChips;
  }
}
