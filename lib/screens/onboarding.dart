// Copyright (C) 2019 Miroslav Mazel
// 
// This file is part of Emparent.
// 
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'package:emparent/database/sphelper.dart';
import 'package:emparent/l10n/l10n.dart';
import 'package:emparent/models/person.dart';
import 'package:emparent/screens/main_navigator.dart';
import 'package:emparent/widgets/page_imagetext.dart';
import 'package:flutter/material.dart';
import 'package:emparent/screens/situation_1_description.dart';
import 'package:emparent/screens/situation_2_emotions.dart';
import 'package:emparent/screens/situation_3_needs.dart';
import 'package:emparent/screens/situation_4_solutions.dart';
import 'package:emparent/models/topic.dart';
import 'package:emparent/models/situation.dart';
import 'package:emparent/screens/page_mixin.dart';
import 'package:emparent/database/dbhelper.dart';
import 'package:flutter/services.dart';

class Onboarding extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return OnboardingState();
  }
}

class OnboardingState extends State<Onboarding> {
  static const int _INIT_PAGE = 0;
  List<Widget> _pages;
  PageController _pageController = PageController(initialPage: _INIT_PAGE);
  double _progress = 0;
  static const String ASSET_FOLDER = "assets/";

  @override
  Widget build(BuildContext context) {
    _pages = <Widget>[
      PageImageText(
        title: L10n.of(context).textIntro1Heading,
        text: L10n.of(context).textIntro1Content,
        bgImage: "${ASSET_FOLDER}onboarding1-c.jpg",
        alignment: Alignment.center,
      ),
      PageImageText(
        title: L10n.of(context).textIntro2Heading,
        text: L10n.of(context).textIntro2Content,
        bgImage: "${ASSET_FOLDER}onboarding2-r.jpg",
        alignment: Alignment.centerRight,
      ),
      PageImageText(
        title: L10n.of(context).textIntro3Heading,
        text: L10n.of(context).textIntro3Content,
        bgImage: "${ASSET_FOLDER}onboarding3-l.jpg",
        alignment: Alignment.centerLeft,
      )
    ];
    return Scaffold(
        body: AnnotatedRegion<SystemUiOverlayStyle>(
            sized: false,
            value: SystemUiOverlayStyle.dark
                .copyWith(statusBarIconBrightness: Brightness.light),
            child: _buildContent()),
        bottomNavigationBar: _getNavigationBar());
  }

  Widget _buildContent() {
    return PageView(
        children: _pages,
        onPageChanged: _onPageChanged,
        controller: _pageController,
        physics: NeverScrollableScrollPhysics());
  }

  Widget _getNavigationBar() {
    TextStyle whiteTheme =
        Theme.of(context).textTheme.button.copyWith(color: Colors.white);
    Widget backButton = (_progress > 0)
        ? FlatButton(
            child: Text(
              L10n.of(context).btnBack,
              style: whiteTheme,
            ),
            onPressed: () {
              _pageController.previousPage(
                  duration: Duration(milliseconds: 300),
                  curve: Cubic(0.0, 0.0, 0.2, 1));
            },
          )
        : FlatButton(
            child: Text(
              L10n.of(context).btnSkipIntro,
              style: whiteTheme,
            ),
            onPressed: () {
              _navigateHome();
            });

    Widget forwardButton = (_progress < 1)
        ? RaisedButton(
            child: Text(L10n.of(context).btnNext),
            onPressed: () {
              _pageController.nextPage(
                  duration: Duration(milliseconds: 300),
                  curve: Cubic(0.0, 0.0, 0.2, 1));
            },
          )
        : RaisedButton(
            child: Text(L10n.of(context).btnDone),
            onPressed: () {
              SPHelper.setShowOnboarding(false);
              _navigateHome();
            },
          );

    return BottomAppBar(
        color: Theme.of(context).colorScheme.background,
        child: Padding(
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              backButton,
              forwardButton,
            ],
          ),
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 0),
        ));
  }

  void _navigateHome() {
    Navigator.pushReplacementNamed(context, MainNavigator.HOME);
  }

  void _onPageChanged(int page) {
    setState(() {
      _progress = page / (_pages.length - 1);
    });
  }
}
