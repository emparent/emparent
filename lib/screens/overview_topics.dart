// Copyright (C) 2019 Miroslav Mazel
//
// This file is part of Emparent.
//
// Emparent is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Emparent is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Emparent.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:io';
import 'package:emparent/database/asset_paths.dart';
import 'package:emparent/l10n/l10n.dart';
import 'package:emparent/models/situation_person.dart';
import 'package:emparent/screens/main_navigator.dart';
import 'package:emparent/widgets/avatar_row.dart';
import 'package:emparent/widgets/people_dialog.dart';
import 'package:flutter/material.dart';
import 'package:emparent/models/topic.dart';
import 'package:emparent/models/situation.dart';
import 'package:emparent/database/dbhelper.dart';
import 'package:emparent/screens/topic_detail.dart';
import 'package:emparent/screens/situation_detail.dart';
import 'package:flutter_mailer/flutter_mailer.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:flutter_email_sender/flutter_email_sender.dart';

// todo

class OverviewTopics extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _OverviewTopicsState();
  }
} //todo refresh after removing a person

class _OverviewTopicsState extends State<OverviewTopics> {
  List<int> _selectedTopicIds = [];
  var hideResolved = false;

  @override
  Widget build(BuildContext context) {
    Widget list = _buildList();

    return Scaffold(
        appBar:
            _isSelectedMode() ? _buildSelectionAppBar() : _buildAppBar(context),
        body: list,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton.extended(
          label: Text(L10n.of(context).btnAddSituation),
          icon: Icon(Icons.add),
          onPressed: () {
            _createSituation();
          },
        ));
  }

  bool _isSelectedMode() {
    return _selectedTopicIds.length > 0;
  }

  Widget _buildSelectionAppBar() {
    return AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            _selectedTopicIds = [];
            setState(() {});
          },
        ),
        title:
            Text(L10n.of(this.context).pluralItems(_selectedTopicIds.length)),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.delete),
            onPressed: () async {
              for (int topicId in _selectedTopicIds) {
                await DBHelper.db.deleteTopic(topicId);
              }
              _selectedTopicIds = [];
              setState(() {});
            },
          )
        ]);
  }

  Widget _buildList() {
    return FutureBuilder<List<DBTopic>>(
        future: hideResolved
            ? DBHelper.db.queryUnresolvedTopics()
            : DBHelper.db.queryAllTopics(), //todo optimize?
        builder: (BuildContext context, AsyncSnapshot<List<DBTopic>> snapshot) {
          if (snapshot.hasData) {
            var topics = snapshot.data;
            if (topics.length > 0)
              return ListView.separated(
                padding: EdgeInsets.only(bottom: 72),
                separatorBuilder: (context, index) => Divider(),
                itemCount: topics.length,
                itemBuilder: (BuildContext context, int position) {
                  return _buildListItem(topics[position]);
                },
              );
            else
              return Padding(
                  padding: EdgeInsets.only(bottom: 72),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                          "${Assets.ASSET_FOLDER}placeholder-logo-white.png"),
                      Text(
                        L10n.of(context).textRecordSituation,
                        softWrap: true,
                        textAlign: TextAlign.center,
                        style: Theme.of(context)
                            .textTheme
                            .headline
                            .copyWith(color: Colors.white30),
                      )
                    ],
                  ));
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
            //todo return empty
          }
        });
  }

  Widget _buildListItem(DBTopic topic) {
    return FutureBuilder(
        future: DBHelper.db.queryTopicSituations(topic.id),
        builder: (context, situationSnapshot) {
          String dateSubtitle = "";
          Widget avatars;
          List<DBSituation> situations = situationSnapshot.data;

          if (situations != null && situations.isNotEmpty) {
            dateSubtitle = DateFormat.yMMMd().format(
                DateTime.fromMillisecondsSinceEpoch(
                    situations.last.dateMilliseconds));
            avatars = FutureBuilder(
                future: DBHelper.db
                    .queryPeopleInSituations(situations.map((DBSituation sit) {
                  return sit.id;
                }).toList()),
                builder: (context, peopleSnapshot) {
                  if (peopleSnapshot.hasData) {
                    List<DBPersonInSituation> peopleInSituation =
                        peopleSnapshot.data;
                    return AvatarRow(
                      people: peopleInSituation.map((var personInSit) {
                        return personInSit.person;
                      }).toList(),
                      radius: 20,
                    );
                  } else
                    return CircularProgressIndicator();
                });
          } else {
            avatars = null;
          }

          Widget text;
          if (topic.title == null || topic.title == "")
            text = Text(
              L10n.of(context).textUntitled,
              style: TextStyle(fontStyle: FontStyle.italic),
            );
          else
            text = Text(topic.title);

          var subtitle = topic.resolved
              ? "$dateSubtitle | ${L10n.of(context).textResolved}"
              : dateSubtitle;

          return ListTile(
            title: text,
            subtitle: Text(subtitle),
            selected: _selectedTopicIds.contains(topic.id),
            onTap: () {
              if (_isSelectedMode())
                _toggleSelection(topic);
              else
                _showDetail(topic);
            },
            onLongPress: () {
              _toggleSelection(topic);
            },
            trailing: avatars,
          );
        });
  }

  void _toggleSelection(DBTopic topic) {
    if (_selectedTopicIds.contains(topic.id)) {
      // unselect
      _selectedTopicIds.remove(topic.id);
    } else {
      // select
      _selectedTopicIds.add(topic.id);
    }
    setState(() {});
  }

  void _showDetail(DBTopic topic) async {
    await Navigator.push(this.context, MaterialPageRoute(builder: (context) {
      return TopicDetail(topic: topic);
    }));
  }

  void _createSituation() async {
    DBTopic topic = await DBHelper.db.createTopic();
    DBSituation situation = await DBHelper.db.createSituation(topic.id);

    await Navigator.push(this.context, MaterialPageRoute(builder: (context) {
      return SituationDetail(topic: topic, situation: situation);
    }));
  }

  // appbar

  static const String _MENU_HIDE_RESOLVED = 'resolved';
  static const String _MENU_PEOPLE = 'People';
  static const String _MENU_RESOURCES = 'Resources';
  static const String _MENU_COMMUNITY = 'Community';
  static const String _MENU_INTRO = 'Intro';
  static const String _MENU_EXPORT = 'Export';
  static const String _MENU_SETTINGS = 'Settings';
  static const String _MENU_FEEDBACK = 'Feedback';

  Widget _buildAppBar(BuildContext context) {
    return AppBar(title: Text('Emparent'), actions: <Widget>[
      PopupMenuButton(
          itemBuilder: (_) => <PopupMenuItem<String>>[
                PopupMenuItem<String>(
                    child: Text(hideResolved
                        ? L10n.of(context).btnShowResolved
                        : L10n.of(context).btnHideResolved),
                    value: _MENU_HIDE_RESOLVED),
                PopupMenuItem<String>(
                    child: Text(L10n.of(context).btnManagePeople),
                    value: _MENU_PEOPLE),
                PopupMenuItem<String>(
                    child: Text(L10n.of(context).btnResources),
                    value: _MENU_RESOURCES),
                PopupMenuItem<String>(
                    child: Text(L10n.of(context).btnCommunity),
                    value: _MENU_COMMUNITY),
                PopupMenuItem<String>(
                    child: Text(L10n.of(context).btnShowIntro),
                    value: _MENU_INTRO),
                PopupMenuItem<String>(
                    child: Text(L10n.of(context)
                        .btnExportData), //todo change functionality!!!
                    value: _MENU_EXPORT),
                // todo PopupMenuItem<String>(
                // child: Text(Intl.message('Nastavení')),
                // value: _MENU_SETTINGS),
                PopupMenuItem<String>(
                    child: Text(L10n.of(context).btnFeedback),
                    value: _MENU_FEEDBACK),
              ],
          onSelected: (String value) {
            switch (value) {
              case _MENU_HIDE_RESOLVED:
                setState(() {
                  hideResolved = !hideResolved;
                });
                break;
              case _MENU_PEOPLE:
                showDialog(context: context, builder: (_) => PeopleDialog());
                break;
              case _MENU_COMMUNITY:
                _showUrl(L10n.of(context).urlCommunity);
                break;
              case _MENU_RESOURCES:
                _showUrl(L10n.of(context).urlResources);
                break;
              case _MENU_INTRO:
                Navigator.pushReplacementNamed(
                    context, MainNavigator.ONBOARDING);
                break;
              case _MENU_EXPORT:
                _email();
                break;
              case _MENU_FEEDBACK:
                _showUrl("mailto:emparent@pm.me");
                break;
            }
          })
    ]);
  }

  void _email() async {
    Directory downloadDirectory = await getTemporaryDirectory();
    String EXPORT_FILE = "MY_DB_EXPORT.db";

    String tempPath = join(downloadDirectory.path, EXPORT_FILE);

    await DBHelper.db.exportDB(tempPath);

    // await DBHelper.db.deleteExportedColumns(tempPath);

    // await FlutterEmailSender.send(email);
    final MailOptions mailOptions = MailOptions(
      // body:
      //     "Dobrý den,\n\nPosílám export záznamů z aplikace Emparent. (V příloze je soubor, z něhož aplikace automaticky odstranila veškeré názvy a popisy situací a nápady, jak situace řešit.)",
      subject: 'Emparent export',
      // recipients: ['emparent@pm.me'],
      // isHTML: true,
      attachments: [
        tempPath,
      ],
    );

    await FlutterMailer.send(mailOptions).catchError(() {
      Scaffold.of(this.context).showSnackBar(SnackBar(
        content: Text(L10n.of(this.context).textCouldNotOpenEmail),
      ));
    });
  }

  // todo too soon? File(tempPath).delete();

  void _showUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
